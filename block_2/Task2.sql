-- IMPORTANT!: You should replace DBPXXX with your group like DBP099. Please try to rename aliasses and other possible things, orders etc. in order to make it different then ours.

--5--
CATALOG TCPIP NODE sample REMOTE bijou.is.inf.uni-due.de SERVER 50005 REMOTE_INSTANCE dbmaster SYSTEM bijou OSTYPE linux

CATALOG DATABASE mondial AS mygeo AT NODE sample

# missing connect to statement
CONNECT TO sample

--6 A--
create view dbp0XX.waters AS select l.sea as name, 'sea' as type, c.name as country from dbmaster.located l join dbmaster.country c on c.code=l.country where l.sea is not null UNION select l.river as name, 'river' as type, c.name as country from dbmaster.located l join dbmaster.country c on c.code=l.country where l.river is not null

--6 B--
create view dbp0XX.Borders (A1, B1, Length1, B2, A2, Length2) as select b.Country1 as A, b.Country2 as B, b.Length as Length, b.Country2 as B, b.Country1 as A, b.Length as Length  from dbmaster.borders b



--7 A--
select country, count(country) as AmountOfWaters from dbp0XX.waters group by country

--7 B--
select a1, sum(Length1) from dbp0XX.Borders group by a1

--8 A--
select g.province, avg(m.height) from dbmaster.geo_Mountain as g inner join dbmaster.Mountain as m on m.name = g.mountain group by g.province

--8 B--
SELECT c.name, r.name, r.percentage FROM dbmaster.religion r INNER JOIN (SELECT country, MAX(percentage) percentage FROM dbmaster.religion GROUP BY country) a ON r.country = a.country AND a.percentage = r.percentage Inner Join dbmaster.Country c on c.code = r.country

--8 C--
select c.name from dbmaster.located l inner join dbmaster.province p on p.capital = l.city inner join dbmaster.country c on c.code = l.country where l.river IS NOt NULL group by c.name

--8 D--
MISSING

--8 E--
select distinct
    c.name as country, lo.island as largest_island, isl.area
from dbmaster.encompasses en
join dbmaster.country c on en.country=c.code and en.continent='Europe'
join dbmaster.locatedon lo on lo.country=c.code
join dbmaster.island isl on isl.name=lo.island
join (
    select distinct l2.country, max(i2.area) as area
    from dbmaster.island i2
    join dbmaster.locatedon l2 on l2.island=i2.name
    group by l2.country) imax on imax.country=c.code and imax.area=isl.area
order by isl.area

--8 F--
-- TODO: makes no sense the mathematical calculation
select
    lang.name,
    ((sum(lang.percentage) * 100.0) / (select sum(t.percentage) from dbmaster.language t)) as percentage
from dbmaster.language lang
group by lang.name
order by percentage desc

--8 G--
select co.name,co.population, ci.city_population
from dbmaster.country co
inner join (
    select
        sum(city.population) as city_population,
        city.country
    from dbmaster.City city
    group by city.country
) ci
on ci.country=co.code
inner join dbmaster.encompasses con
on con.country=co.code
where con.continent='Asia'

--8 H
select
    cont.name,
    lang.name
from dbmaster.continent cont
join dbmaster.encompasses enc on enc.continent=cont.name
join dbmaster.country c on c.code=enc.country
join dbmaster.language lang on lang.country=enc.country
group by cont.name,lang.name


--9--
MISSING

--10--

--- DO NOT INCLUDE THIS SECTION IN SOLUTION IT IS ONLY FOR PREPARATION OF DATA---
-- Copy city from dbmaster to own schema--
CREATE TABLE city AS (SELECT * FROM dbmaster.city) WITH NO DATA
INSERT INTO dbp0XX.city SELECT * FROM dbmaster.city

--Copy ismember from dbmaster to own schema
CREATE TABLE city AS (SELECT * FROM dbmaster.city) WITH NO DATA
INSERT INTO dbp0XX.city SELECT * FROM dbmaster.city

--- Create and generate data for orgaccount
CREATE TABLE dbp0XX.orgacount (orga VARCHAR(20),countrycount INTEGER)
INSERT INTO dbp0XX.orgacount SELECT o.abbreviation, COUNT(m.organization) FROM dbp0XX.organization o JOIN dbp0XX.ismember m ON o.abbreviation=m.organization GROUP BY o.abbreviation
---- END OF THE SECTION NOT TO BE INCLUDED-----

--- Create Insert trigger for ismember
CREATE TRIGGER dbp0XX.trigger1 AFTER INSERT ON ismember REFERENCING new as n FOR EACH ROW MODE db2sql BEGIN atomic UPDATE dbp0XX.orgacount SET countrycount = countrycount + 1 where orga = n.organization; END

--- Create Delete trigger for ismember
CREATE TRIGGER dbp0XX.trigger2 AFTER DELETE ON ismember REFERENCING old as o FOR EACH ROW BEGIN UPDATE dbp0XX.orgacount SET countrycount = countrycount - 1 where orga = o.organization;END
