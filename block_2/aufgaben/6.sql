/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/

/* 6) */
SET SCHEMA dbmaster;

/* Creates a view under dbp022.waters -> selects ALL the waters (including lake) */
CREATE VIEW dbp022.waters AS
SELECT
	data.Name as Name,
	data.Type as Type,
	countries.name as Country
FROM (
	SELECT DISTINCT
		gr.river as Name,
		'river' as Type,
		country
	FROM geo_river gr
	UNION /* If in the task "sea" is really not desired, take this out */
	SELECT DISTINCT
		gs.sea as Name,
		'sea' as Type,
		country
	FROM geo_sea gs /* Or simply just select where not type like '%sea%' from the view*/
	UNION
	SELECT DISTINCT
		gl.lake as Name,
		'lake' as Type,
		country
	FROM geo_lake gl
	) data
JOIN COUNTRY countries
ON countries.code = data.country;

/* Borders */
CREATE VIEW dbp022.borders AS
SELECT
    *
FROM (
    SELECT
        country1 as A,
	country2 B,
	length
    FROM dbmaster.borders
    UNION
    SELECT
        country2 A,
        country1 B,
        length
    FROM dbmaster.borders) data
;

/*  END 6  */