/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/
/* 9 */
WITH Network (River_Name, River_Lake, River_Length) as (
    SELECT name as River_Name, lake as River_Lake, length as River_Length
    FROM dbmaster.river WHERE lake = 'Caspian Sea'
    UNION ALL
    SELECT
        caspian_rivers.name as River_Name,
        caspian_rivers.lake as River_Lake,
        caspian_rivers.length as River_Length
    FROM dbmaster.river as caspian_rivers, Network as net
    WHERE net.River_Name = caspian_rivers.river
) SELECT SUM(River_Length) as Overall_Length FROM Network
/* END 9 */