/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/
/* 5) */
/* Catalog the node */
CATALOG TCPIP NODE dbmaster REMOTE bijou.is.inf.uni-due.de SERVER 50005 REMOTE_INSTANCE dbmaster SYSTEM bijou OSTYPE Linux
/* catalog the database modnial */
CATALOG DATABASE monidal AS MONDB AT NODE dbmaster
/* Connect to it */
CONNECT TO MONDB
/* END 5 */
