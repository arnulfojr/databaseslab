/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/
/* 7)  */
/* COUNT ALL the Water bodies in a country */
SELECT COUNTRY, COUNT(*) FROM dbp022.waters GROUP BY COUNTRY;
/* Sum the overall amount of border found by country */
SELECT B_INI, SUM(LENGTH) AS TOTAL_BORDER FROM dbp022.borders GROUP BY B_INI
/*  END 7   */