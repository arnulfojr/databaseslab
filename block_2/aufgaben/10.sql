/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/
/* 10  */
/* create a local table for city */
CREATE TABLE dbp022.city (
    name varchar(35) NOT NULL,
    country varchar(4) NOT NULL,
    province VARCHAR(35) NOT NULL,
    population INTEGER,
    longitude double,
    latitude double
);
/* create a local table for ismember */
CREATE TABLE dbp022.ismember (
    country varchar(4) NOT NULL,
    organization varchar(12) NOT NULL,
    type varchar(35)
);

/* create the orgaccount table */
CREATE TABLE dbp022.orgaccount (
    orga VARCHAR(20),
    countrycount INTEGER
);
/* mod the mistake if needed */
-- ALTER TABLE dbp022.orgaccount RENAME COLUMN org TO orga;

/* populate the table to the local orgaccount */
INSERT INTO dbp022.orgaccount
    SELECT
        o.abbreviation,
        COUNT(m.organization)
    FROM organization o
    JOIN ismember m ON o.abbreviation = m.organization
    GROUP BY o.abbreviation
;

/* CREATE TWO triggers named triggerX; x being the sequential naming number  */
/* trigger1 => when a new member joins to the table ismember the countrycount should be incremented by one, the country shall match the one from the new member*/

/* INCREMENT TRIGGER */
CREATE TRIGGER dbp022.trigger1
AFTER INSERT ON dbp022.ismember
REFERENCING NEW AS member
FOR EACH ROW MODE DB2SQL
UPDATE dbp022.orgaccount o SET countrycount = countrycount + 1 WHERE orga = member.organization
;
-- SAME TRIGGER (then the one above) BUT BASED IN A TRANSACTION/PROCEDURE:
CREATE TRIGGER dbp022.trigger1
AFTER INSERT ON dbp022.ismember
REFERENCING NEW AS newmember
FOR EACH ROW MODE DB2SQL
BEGIN
    ATOMIC DECLARE counter INTEGER;
    SET counter = (
        SELECT
            countrycount
        FROM dbp022.orgaccount oa
        WHERE oa.orga = newmember.organization
    );
    UPDATE dbp022.orgaccount SET countrycount = counter + 1 WHERE orga = newmember.organization;
END
;
/* trigger2 => when a member from the ismember is deleted then the countrycount shall be decreased */
/* Use the AFTER DELETE and reference to the deleted row with the REFERENCING OLD */
/* Use BEGIN ATOMIC to rollback if the procedure fails */
/* NOT ATOMIC can also be used as we only care for the update statement but as we treat this procedure as a single whole we use atomic */
/* About atomic procedures: http://www.ibm.com/support/knowledgecenter/SSEPGG_9.7.0/com.ibm.db2.luw.apdv.sqlpl.doc/doc/c0024345.html */
CREATE TRIGGER dbp022.trigger2
AFTER DELETE ON dbp022.ismember
REFERENCING OLD AS oldmember
FOR EACH ROW MODE DB2SQL
BEGIN
    ATOMIC DECLARE counter INTEGER;
    SET counter = (
        SELECT
            countrycount
        FROM dbp022.orgaccount oa
        WHERE oa.orga = oldmember.organization
    );
    UPDATE dbp022.orgaccount SET countrycount = counter - 1 WHERE orga = oldmember.organization;
END
;
/* END */