/*
 Databases Laboratory - WS16/17.
 Team 22.
 Arnulfo Solis - 3014693.
 Rauf Abdullayev - 3011671.
*/
/*  START 8  */
/* 8.a */
/* Average height of the mountains per country */
SELECT
    Province,
    AVG(Height) as AVG_HEIGHT
FROM (
    SELECT DISTINCT
        gm.mountain as Mountain,
        gm.province as Province,
        m.height as Height
    FROM geo_mountain gm
    JOIN mountain m
    ON gm.mountain = m.name
) GROUP BY Province
;


/* 8.b */
SELECT
    re.name,
    c.name,
    maxData.Percentage
FROM (
    SELECT
        r.country as Country,
        MAX(r.percentage) as Percentage
    FROM religion r
    GROUP BY r.country
) maxData
JOIN country c ON maxData.Country = c.code
JOIN religion re
ON re.percentage = maxData.Percentage AND maxData.Country = re.country
ORDER BY c.name
;

/* 8.c */
/* SELECT All countries which have capitals of provinces have a river */
--SELECT DISTINCT c.name FROM geo_river gr JOIN country c ON gr.country = c.code JOIN province p ON gr.province = p.name WHERE gr.province = p.capital
SELECT
    country.name
FROM dbmaster.located as located
JOIN dbmaster.province as Province ON Province.capital = located.city
JOIN dbmaster.country as country ON located.country = country.code
WHERE located.river IS NOT NULL GROUP BY country.name
;

/* 8.d */
-- Goes through some logic, last statement is the final answer!
-- Logic: get the symetric relationship in the borders:
SELECT country1 as A, country2 as B FROM borders
UNION SELECT country2 as A, country1 as B FROM borders
;
-- Logic: get a unique relationship between borders:
SELECT DISTINCT A, B FROM (
    SELECT country1 as A, country2 as B FROM borders
    UNION SELECT country2 as A, country1 as B FROM borders
) border
;
-- ANSWER: get the countries that have equal or more than 7 neighbors:
SELECT DISTINCT
    countries.name,
    count(B) as neighbors
FROM (
    SELECT country1 as A, country2 as B FROM borders
    UNION SELECT country2 as A, country1 as B FROM borders
) border
JOIN dbmaster.country countries ON border.A = countries.code
GROUP BY countries.name
HAVING COUNT(B) >= 7
;

/* 8.e */
SELECT DISTINCT
    countries.name AS country,
    locatedon.island as biggest_island,
    islands.area
FROM dbmaster.encompasses as en
JOIN dbmaster.country as countries on en.country = countries.code AND en.continent = 'Europe'
JOIN dbmaster.locatedon as locatedon on locatedon.country = countries.code
JOIN dbmaster.island as islands on islands.name = locatedon.island
JOIN (
    SELECT DISTINCT
        locatedin.country as country,
        max(myislands.area) as area
    FROM dbmaster.island as myislands
    JOIN dbmaster.locatedon as locatedin on locatedin.island = myislands.name
    GROUP BY locatedin.country
) biggest on biggest.country = countries.code AND biggest.area = islands.area
ORDER BY islands.area
;

/* 8.f */
-- Goes through some logic, last statement is the final answer!
-- get the number of persons in the world that speak this language
SELECT
    lang.name as name,
    SUM(lang.percentage * c.population / 100) as people
FROM dbmaster.language as lang
INNER JOIN dbmaster.country c ON lang.country = c.code
GROUP BY lang.name
;
-- Now we have the number of persons that speak that language, get the percentage of speakers in the world
    -- get the amount of people in the world
SELECT SUM(CAST(c.population as DOUBLE)) as all_people FROM dbmaster.country as c;
-- ANSWER: Now we have the number of people in the world and the number of speakers
    -- Used Decimal to convert from floating point to fixed point numbers
SELECT
    languages.language,
    DECIMAL((languages.speakers * 100 / everybody.world), 7, 4) as percentage
FROM (
    SELECT
        lang.name as language,
        SUM(lang.percentage * c.population / 100) as speakers
    FROM dbmaster.language as lang
    INNER JOIN dbmaster.country c ON lang.country = c.code
    GROUP BY lang.name
) languages, (
    SELECT
        SUM(CAST(c.population as DOUBLE)) as world
    FROM dbmaster.country as c
) everybody
;

/* 8.g */
-- Goes through some logic, last statement is the final answer!
-- get all countries from asia
SELECT
    countries.name as country,
    countries.code as code
FROM dbmaster.encompasses AS continents
JOIN dbmaster.country as countries
ON continents.country = countries.code
WHERE continents.continent = 'Asia'
;
-- get number of people in ALL cities
SELECT
    SUM(cities.population) as people,
    cities.country
FROM dbmaster.city as cities
GROUP BY cities.country
;
-- NOW inner join them! to have a country - number of people relationship
-- this gives the total amount of people per country
SELECT
    asian_countries.country as country,
    asian_countries.code as code,
    people.people as people
FROM (
    SELECT
        countries.name as country,
        countries.code as code
    FROM dbmaster.encompasses AS continents
    JOIN dbmaster.country as countries
    ON continents.country = countries.code
    WHERE continents.continent = 'Asia'
) asian_countries
INNER JOIN (
    SELECT
        cities.country as country,
        SUM(cities.population) as people
    FROM dbmaster.city as cities
    GROUP BY cities.country
) people ON asian_countries.code = people.country WHERE people IS NOT NULL
;
-- Now we need to calculate the percentage relative to the total amount of people in the world
-- used this condition to avoid the lack of Macau data (i.E.)
-- Now that we know the amount of people in each Asian country we need the percentage of people
-- per city relative to the countries total population
-- get the cities of Asia
SELECT
    asian_cities.name as city,
    asian_cities.population as people,
    asian_countries.country as country,
    asian_countries.code as country_code
FROM dbmaster.city as asian_cities
INNER JOIN (
    SELECT
        countries.name as country,
        countries.code as code
    FROM dbmaster.encompasses AS continents
    JOIN dbmaster.country as countries
    ON continents.country = countries.code
    WHERE continents.continent = 'Asia'
) asian_countries ON asian_cities.country = asian_countries.code
WHERE asian_cities.population IS NOT NULL
;
-- IS NOT NULL to not take in consideration the cities with no data
-- Now we have a table with the amount of people per country and amount of people per city FROM ASIA!
-- JOIN THEM FTW!
-- ANSWER: NOW we can do the mathematical calculations --> percentage of people in the city relative to the countrie's population
SELECT
    asian_countries.country,
    asian_cities.city,
    asian_cities.people as people_ci,
    asian_countries.people as people_co,
    DECIMAL((asian_cities.people * 100.0 / asian_countries.people), 6, 2) as percentage
FROM (
    SELECT
        asian_countries.country as country,
        asian_countries.code as code,
        people.people as people
    FROM (
        SELECT
            countries.name as country,
            countries.code as code
        FROM dbmaster.encompasses AS continents
        JOIN dbmaster.country as countries
        ON continents.country = countries.code
        WHERE continents.continent = 'Asia'
    ) asian_countries
    INNER JOIN (
        SELECT
            cities.country as country,
            SUM(cities.population) as people
        FROM dbmaster.city as cities
        GROUP BY cities.country
    ) people ON asian_countries.code = people.country WHERE people IS NOT NULL
) asian_countries
INNER JOIN (
    SELECT
        asian_cities.name as city,
        asian_cities.population as people,
        asian_countries.country as country,
        asian_countries.code as country_code
    FROM dbmaster.city as asian_cities
    INNER JOIN (
        SELECT
            countries.name as country,
            countries.code as code
        FROM dbmaster.encompasses AS continents
        JOIN dbmaster.country as countries
        ON continents.country = countries.code
        WHERE continents.continent = 'Asia'
    ) asian_countries ON asian_cities.country = asian_countries.code
    WHERE asian_cities.population IS NOT NULL
) asian_cities ON asian_countries.code = asian_cities.country_code
ORDER BY asian_countries.country
;
/* 8.h */
-- have a table where the country - continent - language relationship is
-- List the languages that are spoken in at least one country per continent
-- We got no results in this query, maybe we missunderstood the instructions...
SELECT
    lang.name as language
FROM dbmaster.language as lang
JOIN dbmaster.encompasses as en ON en.country = lang.country
JOIN dbmaster.country as countries ON lang.country = countries.code
GROUP BY lang.name
HAVING COUNT(DISTINCT en.continent) >= (
    SELECT COUNT(*) FROM dbmaster.continent
)
;
-- It counts the number of continents the language is spoken on, if that number is greater or equal than the number of continents estimated, then OK it is spoken in at least once per continent


/* END 8 */