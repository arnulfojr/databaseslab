-- TODO: use spaces instead of tabs

/* 5) */
/* Catalog the node */
CATALOG TCPIP NODE dbmaster REMOTE bijou.is.inf.uni-due.de SERVER 50005 REMOTE_INSTANCE dbmaster SYSTEM bijou OSTYPE Linux
/* catalog the database modnial */
CATALOG DATABASE monidal AS MONDB AT NODE dbmaster
/* Connect to it */
CONNECT TO MONDB
/* END 5 */

/* 6) */
SET SCHEMA dbmaster;
-- go better for the prettiefied
SELECT DISTINCT e.river as Name, 'river' AS Type, c.name AS Country FROM geo_river e join COUNTRY c ON c.code = e.country UNION SELECT DISTINCT g.sea as Name, 'sea' AS Type, c.name AS Country FROM geo_sea g join  COUNTRY c ON c.code = g.country UNION SELECT DISTINCT g.lake as Name, 'lake' AS Type, c.name AS Country FROM   geo_lake g join  COUNTRY c ON c.code = g.country
-- Go better for the prettyfied
CREATE VIEW dbp022.waters AS SELECT data.Name as Name, data.Type as Type, countries.name as Country FROM (SELECT DISTINCT gr.river as Name, 'river' as Type, country FROM geo_river gr UNION SELECT DISTINCT gs.sea as Name, 'sea' as Type, country FROM geo_sea gs UNION SELECT DISTINCT gl.lake as Name, 'lake' as Type, country FROM geo_lake gl) data JOIN COUNTRY countries ON countries.code = data.country

/* Creates a view under dbp022.waters -> selects ALL the waters (including lake) */
CREATE VIEW dbp022.waters AS
SELECT
	data.Name as Name,
	data.Type as Type,
	countries.name as Country
FROM (
	SELECT DISTINCT
		gr.river as Name,
		'river' as Type,
		country
	FROM geo_river gr
	UNION /* If in the task "sea" is really not desired, take this out */
	SELECT DISTINCT
		gs.sea as Name,
		'sea' as Type,
		country
	FROM geo_sea gs /* Or simply just select where not type like '%sea%' from the view*/
	UNION
	SELECT DISTINCT
		gl.lake as Name,
		'lake' as Type,
		country
	FROM geo_lake gl
	) data
JOIN COUNTRY countries
ON countries.code = data.country


/* symmetric border view */
CREATE VIEW dbp022.borders AS SELECT * FROM (SELECT country1 as b_ini, country2 b_end, length FROM borders UNION SELECT country2 b_ini, country1 b_end, length FROM borders) data WHERE data.b_ini = 'MEX';
/* Prettified: */
CREATE VIEW dbp022.borders AS
SELECT
    *
FROM (
    SELECT
        country1 as A,
	country2 B,
	length
    FROM dbmaster.borders
    UNION
    SELECT
        country2 A,
        country1 B,
        length
    FROM dbmaster.borders) data
;

/*  END 6  */

/* 7)  */
/* COUNT ALL the Water bodies in a country */
SELECT COUNTRY, COUNT(*) FROM dbp022.waters GROUP BY COUNTRY;
/* Sum the overall amount of border found by country */
SELECT B_INI, SUM(LENGTH) AS TOTAL_BORDER FROM dbp022.borders GROUP BY B_INI

/*  END 7   */

/*  START 8  */
/* 8.a */
/* Average height of the mountains per country */
SELECT
    Province,
    AVG(Height) as AVG_HEIGHT
FROM (
    SELECT DISTINCT
        gm.mountain as Mountain,
        gm.province as Province,
        m.height as Height
    FROM geo_mountain gm
    JOIN mountain m
    ON gm.mountain = m.name
) GROUP BY Province


/* 8.b */
SELECT
    re.name,
    c.name,
    maxData.Percentage
FROM (
    SELECT
        r.country as Country,
        MAX(r.percentage) as Percentage
    FROM religion r
    GROUP BY r.country
) maxData
JOIN country c ON maxData.Country = c.code
JOIN religion re
ON re.percentage = maxData.Percentage AND maxData.Country = re.country
ORDER BY c.name


/* 8.c */
/* SELECT All countries which have capitals of provinces have a river */
--SELECT DISTINCT c.name FROM geo_river gr JOIN country c ON gr.country = c.code JOIN province p ON gr.province = p.name WHERE gr.province = p.capital
SELECT
    country.name
FROM dbmaster.located as located
JOIN dbmaster.province as Province ON Province.capital = located.city
JOIN dbmaster.country as country ON located.country = country.code
WHERE located.river IS NOT NULL GROUP BY country.name

/* 8.d */
-- Goes through some logic, last statement is the final answer!
-- Logic: get the symetric relationship in the borders:
SELECT country1 as A, country2 as B FROM borders
UNION SELECT country2 as A, country1 as B FROM borders
-- Logic: get a unique relationship between borders:
SELECT DISTINCT A, B FROM (
    SELECT country1 as A, country2 as B FROM borders
    UNION SELECT country2 as A, country1 as B FROM borders
) border
-- ANSWER: get the countries that have equal or more than 7 neighbors:
SELECT DISTINCT
    countries.name,
    count(B) as neighbors
FROM (
    SELECT country1 as A, country2 as B FROM borders
    UNION SELECT country2 as A, country1 as B FROM borders
) border
JOIN dbmaster.country countries ON border.A = countries.code
GROUP BY countries.name
HAVING COUNT(B) >= 7
;

/* 8.e */
SELECT DISTINCT
    countries.name AS country,
    locatedon.island as biggest_island,
    islands.area
FROM dbmaster.encompasses as en
JOIN dbmaster.country as countries on en.country = countries.code AND en.continent = 'Europe'
JOIN dbmaster.locatedon as locatedon on locatedon.country = countries.code
JOIN dbmaster.island as islands on islands.name = locatedon.island
JOIN (
    SELECT DISTINCT
        locatedin.country as country,
        max(myislands.area) as area
    FROM dbmaster.island as myislands
    JOIN dbmaster.locatedon as locatedin on locatedin.island = myislands.name
    GROUP BY locatedin.country
) biggest on biggest.country = countries.code AND biggest.area = islands.area
ORDER BY islands.area
;

/* 8.f */
-- Goes through some logic, last statement is the final answer!
-- get the number of persons in the world that speak this language
SELECT
    lang.name as name,
    SUM(lang.percentage * c.population / 100) as people
FROM dbmaster.language as lang
INNER JOIN dbmaster.country c ON lang.country = c.code
GROUP BY lang.name;
-- Now we have the number of persons that speak that language, get the percentage of speakers in the world
    -- get the amount of people in the world
SELECT SUM(CAST(c.population as DOUBLE)) as all_people FROM dbmaster.country as c;
-- ANSWER: Now we have the number of people in the world and the number of speakers
    -- Used Decimal to convert from floating point to fixed point numbers
SELECT
    languages.language,
    DECIMAL((languages.speakers * 100 / everybody.world), 7, 4) as percentage
FROM (
    SELECT
        lang.name as language,
        SUM(lang.percentage * c.population / 100) as speakers
    FROM dbmaster.language as lang
    INNER JOIN dbmaster.country c ON lang.country = c.code
    GROUP BY lang.name
) languages, (
    SELECT
        SUM(CAST(c.population as DOUBLE)) as world
    FROM dbmaster.country as c
) everybody
;

/* 8.g */
-- Goes through some logic, last statement is the final answer!
-- get all countries from asia
SELECT
    countries.name as country,
    countries.code as code
FROM dbmaster.encompasses AS continents
JOIN dbmaster.country as countries
ON continents.country = countries.code
WHERE continents.continent = 'Asia'
-- get number of people in ALL cities
SELECT
    SUM(cities.population) as people,
    cities.country
FROM dbmaster.city as cities
GROUP BY cities.country
-- NOW inner join them! to have a country - number of people relationship
-- this gives the total amount of people per country
SELECT
    asian_countries.country as country,
    asian_countries.code as code,
    people.people as people
FROM (
    SELECT
        countries.name as country,
        countries.code as code
    FROM dbmaster.encompasses AS continents
    JOIN dbmaster.country as countries
    ON continents.country = countries.code
    WHERE continents.continent = 'Asia'
) asian_countries
INNER JOIN (
    SELECT
        cities.country as country,
        SUM(cities.population) as people
    FROM dbmaster.city as cities
    GROUP BY cities.country
) people ON asian_countries.code = people.country WHERE people IS NOT NULL
-- Now we need to calculate the percentage relative to the total amount of people in the world
-- used this condition to avoid the lack of Macau data (i.E.)
-- Now that we know the amount of people in each Asian country we need the percentage of people
-- per city relative to the countries total population
-- get the cities of Asia
SELECT
    asian_cities.name as city,
    asian_cities.population as people,
    asian_countries.country as country,
    asian_countries.code as country_code
FROM dbmaster.city as asian_cities
INNER JOIN (
    SELECT
        countries.name as country,
        countries.code as code
    FROM dbmaster.encompasses AS continents
    JOIN dbmaster.country as countries
    ON continents.country = countries.code
    WHERE continents.continent = 'Asia'
) asian_countries ON asian_cities.country = asian_countries.code
WHERE asian_cities.population IS NOT NULL
-- IS NOT NULL to not take in consideration the cities with no data
-- Now we have a table with the amount of people per country and amount of people per city FROM ASIA!
-- JOIN THEM FTW!
-- ANSWER: NOW we can do the mathematical calculations --> percentage of people in the city relative to the countrie's population
SELECT
    asian_countries.country,
    asian_cities.city,
    asian_cities.people as people_ci,
    asian_countries.people as people_co,
    DECIMAL((asian_cities.people * 100.0 / asian_countries.people), 6, 2) as percentage
FROM (
    SELECT
        asian_countries.country as country,
        asian_countries.code as code,
        people.people as people
    FROM (
        SELECT
            countries.name as country,
            countries.code as code
        FROM dbmaster.encompasses AS continents
        JOIN dbmaster.country as countries
        ON continents.country = countries.code
        WHERE continents.continent = 'Asia'
    ) asian_countries
    INNER JOIN (
        SELECT
            cities.country as country,
            SUM(cities.population) as people
        FROM dbmaster.city as cities
        GROUP BY cities.country
    ) people ON asian_countries.code = people.country WHERE people IS NOT NULL
) asian_countries
INNER JOIN (
    SELECT
        asian_cities.name as city,
        asian_cities.population as people,
        asian_countries.country as country,
        asian_countries.code as country_code
    FROM dbmaster.city as asian_cities
    INNER JOIN (
        SELECT
            countries.name as country,
            countries.code as code
        FROM dbmaster.encompasses AS continents
        JOIN dbmaster.country as countries
        ON continents.country = countries.code
        WHERE continents.continent = 'Asia'
    ) asian_countries ON asian_cities.country = asian_countries.code
    WHERE asian_cities.population IS NOT NULL
) asian_cities ON asian_countries.code = asian_cities.country_code
ORDER BY asian_countries.country

/* 8.h */
-- have a table where the country - continent - language relationship is
-- List the languages that are spoken in at least one country per continent
-- We got no results in this query, maybe we missunderstood the instructions...
SELECT
    lang.name as language
FROM dbmaster.language as lang
JOIN dbmaster.encompasses as en ON en.country = lang.country
JOIN dbmaster.country as countries ON lang.country = countries.code
GROUP BY lang.name
HAVING COUNT(DISTINCT en.continent) >= (
    SELECT COUNT(*) FROM dbmaster.continent
)
-- It counts the number of continents the language is spoken on, if that number is greater or equal than the number of continents estimated, then OK it is spoken in at least once per continent


/* END 8 */

/* 9 */
WITH Network (River_Name, River_Lake, River_Length) as (
    SELECT name as River_Name, lake as River_Lake, length as River_Length
    FROM dbmaster.river WHERE lake = 'Caspian Sea'
    UNION ALL
    SELECT
        caspian_rivers.name as River_Name,
        caspian_rivers.lake as River_Lake,
        caspian_rivers.length as River_Length
    FROM dbmaster.river as caspian_rivers, Network as net
    WHERE net.River_Name = caspian_rivers.river
) SELECT SUM(River_Length) as Overall_Length FROM Network
/* END 9 */


/* 10  */
/* create a local table for city */
CREATE TABLE dbp022.city (name varchar(35) NOT NULL,country varchar(4) NOT NULL, province VARCHAR(35) NOT NULL,population INTEGER, longitude double, latitude double);
/* create a local table for ismember */
CREATE TABLE dbp022.ismember (country varchar(4) NOT NULL, organization varchar(12) not null, type varchar(35));

/* create the orgaccount table */
CREATE TABLE dbp022.orgaccount (
    orga VARCHAR(20),
    countrycount INTEGER
);
/* mod the mistake if needed */
-- ALTER TABLE dbp022.orgaccount RENAME COLUMN org TO orga;

/* populate the table to the local orgaccount */
INSERT INTO dbp022.orgaccount SELECT o.abbreviation, COUNT(m.organization) FROM organization o JOIN ismember m ON o.abbreviation = m.organization GROUP BY o.abbreviation;

/* CREATE TWO triggers named triggerX; x being the sequential naming number  */
/* trigger1 => when a new member joins to the table ismember the countrycount should be incremented by one, the country shall match the one from the new member*/

/* INCREMENT TRIGGER */
CREATE TRIGGER dbp022.trigger1
AFTER INSERT ON dbp022.ismember
REFERENCING NEW AS member
FOR EACH ROW MODE DB2SQL
UPDATE dbp022.orgaccount o SET countrycount = countrycount + 1 WHERE orga = member.organization
-- SAME TRIGGER BUT BASED IN A TRANSACTION/PROCEDURE:
CREATE TRIGGER dbp022.trigger1
AFTER INSERT ON dbp022.ismember
REFERENCING NEW AS newmember
FOR EACH ROW MODE DB2SQL
BEGIN
    ATOMIC DECLARE counter INTEGER;
    SET counter = (
        SELECT
            countrycount
        FROM dbp022.orgaccount oa
        WHERE oa.orga = newmember.organization
    );
    UPDATE dbp022.orgaccount SET countrycount = counter + 1 WHERE orga = newmember.organization;
END
/* trigger2 => when a member from the ismember is deleted then the countrycount shall be decreased */
/* Use the AFTER DELETE and reference to the deleted row with the REFERENCING OLD */
/* Use BEGIN ATOMIC to rollback if the procedure fails */
/* NOT ATOMIC can also be used as we only care for the update statement but as we treat this procedure as a single whole we use atomic */
/* About atomic procedures: http://www.ibm.com/support/knowledgecenter/SSEPGG_9.7.0/com.ibm.db2.luw.apdv.sqlpl.doc/doc/c0024345.html */
CREATE TRIGGER dbp022.trigger2
AFTER DELETE ON dbp022.ismember
REFERENCING OLD AS oldmember
FOR EACH ROW MODE DB2SQL
BEGIN
    ATOMIC DECLARE counter INTEGER;
    SET counter = (
        SELECT
            countrycount
        FROM dbp022.orgaccount oa
        WHERE oa.orga = oldmember.organization
    );
    UPDATE dbp022.orgaccount SET countrycount = counter - 1 WHERE orga = oldmember.organization;
END

/* END */
