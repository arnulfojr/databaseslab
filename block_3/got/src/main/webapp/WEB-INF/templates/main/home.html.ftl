<#import "../base.html.ftl" as b>

<@b.base>
    <@b.nav>
        <li>
            <a href="/logout">Logout</a>
        </li>
    </@b.nav>
<div class="container animated fadeIn" ng-app="gotApp">
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="block-center">
                                <img src="images/header.png" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h2>Welcome to the GoT Metadata browser</h2>
                            <p>This website is aimed to have a comprehensive view and navigation of the GoT database.</p>
                            <p>Group 22</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ng-view></ng-view>

</div>
<!-- scripts -->
<script src="lib/angular/angular.min.js" type="application/javascript"></script>
<script src="lib/angular-route/angular-route.min.js" type="application/javascript"></script>
<script src="assets/js/services/endpoint.js" type="application/javascript"></script>
<script src="assets/js/directives/alerts.js" type="application/javascript"></script>
<script src="assets/js/directives/listings.js" type="application/javascript"></script>
<script src="assets/js/main/app.js" type="application/javascript"></script>
<!-- end scripts -->
</@b.base>