<#macro base>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GoT</title>

        <link href="lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="lib/bootswatch/flatly/bootstrap.min.css" rel="stylesheet"/>
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="lib/animate.css/animate.min.css" rel="stylesheet"/>

        <script src="lib/jquery/dist/jquery.min.js" type="application/javascript"></script>
        <script src="lib/bootstrap/dist/js/bootstrap.min.js" type="application/javascript"></script>

        <base href="/">

        <style>
            html {
                margin-bottom: 120px; /* Give some spacing with the end of the document */
            }
        </style>

    </head>
    <body>
        <#nested>
        <!-- end Nav bar -->
    </body>
</html>
</#macro>

<#macro nav>
<!-- Nav bar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">
                <span class="fa fa-home"></span>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <#nested>
            </ul>
        </div>
    </div>
</nav>
</#macro>