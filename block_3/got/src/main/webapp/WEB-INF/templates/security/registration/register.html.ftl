<#import "../../base.html.ftl" as b>

<@b.base>
    <@b.nav>
        <li>
            <a href="/login">Log in</a>
        </li>
    </@b.nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form class="form-horizontal" action="/register" method="post">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="firstname">First Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="firstname" id="firstname"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="lastname">Last Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="lastname" id="lastname"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="username">Username</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="username" id="username"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="password">Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="password" name="password" id="password"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary" type="submit">Register</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@b.base>