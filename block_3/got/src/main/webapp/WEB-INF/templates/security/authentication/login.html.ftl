<#import "../../base.html.ftl" as b>

<@b.base>
    <@b.nav>
    <li>
        <a href="/register">Register</a>
    </li>
    </@b.nav>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <p class="">${message}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form class="form-horizontal" action="/login" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="username">Username</label>
                            <div class="col-sm-10">
                                <input id="username" name="username" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="password">Password</label>
                            <div class="col-sm-10">
                                <input id="password" name="password" type="password" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@b.base>