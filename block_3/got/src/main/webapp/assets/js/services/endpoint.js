var endpoint = angular.module("endpoint", []);

/**
 * figures end point communicator
 */
endpoint.service("api", ["$http", "$q", function($http, $q) {

    const API_ENDPOINT = "/api";
    const SEARCH_URI = "/search";
    const FIGURES_URI = "/figures";
    const GUESS_URI = "/guess";
    const ANIMAL_URI = "/animals";
    const PERSON_URI = "/persons";
    const PLAYLIST_URI = "/playlists";
    const SEASONS_URI = "/seasons";
    const HOUSES_URI = "/houses";
    const EPISODES_URI = "/episodes";
    const LOCATION_URI = "/locations";
    const RULED_URI = "/ruled";
    const MEMBERS_URI = "/members";

    var defObject;

    /**
     * Returns a promise with a list of figures
     * @param limit
     * @returns Promise
     */
    this.getFigures = function(limit) {

        limit = (limit == undefined) ? -1 : limit;

        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + FIGURES_URI;

        var payload = {
            limit: limit
        };

        $http({
            method: "GET",
            url: url,
            params: payload
        }).then(function(data) {
            defObj.resolve(data.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Searches for the keyword
     * @param keyword
     * @returns Promise
     */
    this.searchFigure = function(keyword) {

        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + SEARCH_URI + FIGURES_URI;

        var payload = {
            keyword: keyword
        };

        $http({
            method: "GET",
            url: url,
            params: payload
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error)
        });

        return defObj.promise;
    };

    /**
     * Returns a list of figures based on the episode
     * @param episodeId
     * @returns Promise
     */
    this.getFiguresFromEpisode = function (episodeId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + FIGURES_URI;
        var payload = {
            episodeId: episodeId
        };

        $http({
            method: "GET",
            url: url,
            params: payload
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns a list of figures based on the parameters passed
     * @param params
     * @returns Promise
     */
    this.getFiguresFrom = function (params) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + FIGURES_URI;
        $http({
            url: url,
            method: "GET",
            params: params
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns an specific object whether the Id represents an Animal or a Person
     *
     * @param specificId
     * @return Promise
     */
    this.guessSpecificFigure = function(specificId) {
        var defObj = defObject || $q.defer();

        var params = {
            specificId: specificId
        };

        var url = API_ENDPOINT + FIGURES_URI + GUESS_URI;

        $http({
            method: "GET",
            params: params,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns the animal with the given Id
     *
     * @param animalId
     * @returns Promise
     */
    this.getAnimal = function(animalId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + FIGURES_URI + ANIMAL_URI;
        var payload = {
            animalId: animalId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns the person with the given person Id
     *
     * @param personId
     * @returns Promise
     */
    this.getPerson = function(personId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + FIGURES_URI + PERSON_URI;
        var payload = {
            personId: personId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns a promise holding a list of seasons
     * @param limit
     * @returns Promise
     */
    this.getSeasons = function(limit) {

        limit = (limit == undefined) ? -1 : limit;
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + SEASONS_URI;
        var payload = {limit: limit};

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(err) {
            defObj.reject(err);
        });

        return defObj.promise;
    };

    this.getSeason = function(seasonId) {
        var defObj = defObject || $q.defer();

        var payload = {
            seasonId: seasonId
        };

        var url = API_ENDPOINT + SEASONS_URI;

        $http({
            method: "get",
            url: url,
            params: payload
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Gets the results of the search of the keyword
     * @param keyword
     * @returns Promise
     */
    this.searchSeason = function(keyword) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + SEARCH_URI + SEASONS_URI;
        var payload = {
            keyword: keyword
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns a promise of a list of playlist(s)
     * @param limit
     * @param catalog
     * @returns Promise
     */
    this.getPlaylists = function (limit, catalog) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + PLAYLIST_URI;
        var payload = {
            limit: limit,
            catalog: catalog
        };
        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns the information of the playlist
     * @param playlistId
     * @param catalog
     * @returns Promise
     */
    this.getPlaylist = function(playlistId, catalog) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + PLAYLIST_URI;

        var payload = {
            catalog: catalog,
            playlistId: playlistId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    this.createPlaylist = function(title) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + PLAYLIST_URI;
        var payload = {
            title: title
        };

        $http({
            method: "POST",
            url: url,
            data: payload,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns a promise holding a list of houses
     * @param limit
     * @returns Promise
     */
    this.getHouses = function(limit) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + HOUSES_URI;
        limit = (limit == undefined) ? -1 : limit;
        var payload = {
            limit: limit
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Gets houses based on the params passed
     * @param params
     * @returns Promise
     */
    this.getHousesFrom = function (params) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + HOUSES_URI;

        $http({
            method: "GET",
            params: params,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Searches the house based on the keyword
     * @param keyword
     * @returns Promise
     */
    this.searchHouse = function(keyword) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + SEARCH_URI + HOUSES_URI;
        var payload = {
            keyword: keyword
        };

        $http({
            method: "GET",
            url: url,
            params: payload
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns the members of the houses
     *
     * @param houseId
     * @returns Promise
     */
    this.getHouseMembers = function(houseId) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + HOUSES_URI + MEMBERS_URI;

        var payload = {
            houseId: houseId
        };

        $http({
            url: url,
            params: payload,
            method: "GET"
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns the list of membership the person has/had
     *
     * @param personId
     * @return Promise
     */
    this.getMemberships = function(personId) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + HOUSES_URI + MEMBERS_URI;

        var payload = {
            personId: personId
        };

        $http({
            url: url,
            params: payload,
            method: "GET"
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Gets the episode data
     * @param episodeId
     * @returns Promise
     */
    this.getEpisode = function(episodeId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + EPISODES_URI;
        var payload = {
            episodeId: episodeId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Returns a list of episodes based on the parameters given
     * @param params
     * @returns Promise
     */
    this.getEpisodesFrom = function (params) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + EPISODES_URI;
        $http({
            method: "GET",
            params: params,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });
        return defObj.promise;
    };

    /**
     * Gets the location based on its Id
     * @param locationId
     * @returns Promise
     */
    this.getLocation = function(locationId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + LOCATION_URI;

        var payload = {
            locationId: locationId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     *
     * Returns the ruled locations
     *
     * @param houseId
     * @returns Promise
     */
    this.getRuledLocationsFromHouse = function(houseId) {
        var defObj = defObject || $q.defer();

        var url = API_ENDPOINT + LOCATION_URI + RULED_URI;

        var payload = {
            houseId: houseId
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Gets all the locations
     * @param limit
     * @returns Promise
     */
    this.getLocations = function(limit) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + LOCATION_URI;

        var payload = {
            limit: limit
        };

        $http({
            method: "GET",
            params: payload,
            url: url
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

    /**
     * Gets the locations based on an episodeId
     * @param episodeId
     * @returns Promise
     */
    this.getLocationsFromEpisode = function(episodeId) {
        var defObj = defObject || $q.defer();
        var url = API_ENDPOINT + LOCATION_URI;

        var payload = {
            episodeId: episodeId
        };

        $http({
            method: "GET",
            url: url,
            params: payload
        }).then(function(response) {
            defObj.resolve(response.data);
        }, function(error) {
            defObj.reject(error);
        });

        return defObj.promise;
    };

}]);
