var gotApp = angular.module("gotApp", ["ngRoute", "listingDirectives", "endpoint", "alerts"]);

gotApp.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/home", {
            templateUrl: "/assets/extra/templates/home.html",
            controller: "homeController"
        })
        .when("/seasons/:seasonId", {
            templateUrl: "/assets/extra/templates/season.html",
            controller: "seasonController"
        })
        .when("/episodes/:episodeId", {
            templateUrl: "/assets/extra/templates/episode.html",
            controller: "episodeController"
        })
        .when("/locations/:locationId", {
            templateUrl: "/assets/extra/templates/location.html",
            controller: "locationController"
        })
        .when("/playlists/:playlistId", {
            templateUrl: "/assets/extra/templates/playlist.html",
            controller: "playlistController"
        })
        .when("/houses/:houseId", {
            templateUrl: "/assets/extra/templates/house.html",
            controller: "houseController"
        })
        .when("/figures/:figureId", {
            controller: "figureController",
            template: " "
        })
        .when("/animals/:animalId", {
            controller: "animalController",
            templateUrl: "/assets/extra/templates/figures/animal.html"
        })
        .when("/persons/:personId", {
            controller: "personController",
            templateUrl: "/assets/extra/templates/figures/person.html"
        })
    ;

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
});

gotApp.controller("animalController", ["$scope", "$routeParams", "api", function($scope, $routeParams, api) {

    $scope.animal = undefined;

    $scope.fetch = function(animalId) {
        api.getAnimal(animalId).then(function(data) {
            console.log(data);
            $scope.animal = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.animalId);

}]);

gotApp.controller("personController", ["$scope", "$routeParams", "api", function($scope, $routeParams, api) {

    $scope.person = undefined;
    $scope.memberships = [];

    $scope.fetch = function(personId) {
        // get the person
        api.getPerson(personId).then(function(data) {
            console.log(data);
            $scope.person = data;
        }, function(error) {
            console.error(error);
        });
        // get the memberships from the personId
        api.getMemberships(personId).then(function(data) {
            console.log(data);
            $scope.memberships = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.personId);

}]);

gotApp.controller("figureController", ["$scope", "$location", "$routeParams", "api", function($scope, $location, $routeParams, api) {

    $scope.decideFromId = function(figureId) {
        api.guessSpecificFigure(figureId).then(function(data) {
            console.log(data);
            if ("animalId" in data)
            {
                // it is an animal
                $location.path("/animals/" + data["animalId"]).replace();
            } else if ("personId" in data) {
                // it is a person
                $location.path("/persons/" + data["personId"]).replace();
            } else {
                console.error("the figureId seems to not exist in the db");
            }

        }, function(error) {
            console.error(error);
        });
    };

    $scope.decideFromId($routeParams.figureId);

}]);

gotApp.controller("seasonController", ["$scope", "api", "$routeParams", function($scope, api, $routeParams) {

    $scope.season = undefined;

    $scope.fetch = function (seasonId) {
        api.getSeason(seasonId).then(function(data) {
            console.log(data);
            $scope.season = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.seasonId);

}]);

gotApp.controller("houseController", ["$scope", "$routeParams", "api", function($scope, $routeParams, api) {

    $scope.house = undefined;
    $scope.ruledLocations = [];
    $scope.members = [];

    $scope.fetch = function(houseId) {
        // get the house information
        api.getHousesFrom({houseId: houseId}).then(function(data) {
            console.log(data);
            $scope.house = data;
        }, function(error) {
            console.error(error);
        });
        // get the ruled locations
        api.getRuledLocationsFromHouse(houseId).then(function(data) {
            $scope.ruledLocations = data;
        }, function(error) {
            console.error(error);
        });
        // get the house members
        api.getHouseMembers(houseId).then(function(data) {
            $scope.members = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.houseId);

}]);

gotApp.controller("playlistController", ["$scope", "api", "$routeParams", function($scope, api, $routeParams) {

    $scope.playlist = undefined;

    $scope.fetch = function(playlistId) {
        api.getPlaylist(playlistId, false).then(function(data) {
            $scope.playlist = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.playlistId);

}]);

gotApp.controller("locationController", ["$scope", "api", "$routeParams", function($scope, api, $routeParams) {

    $scope.location = undefined;
    $scope.figures = [];
    $scope.episodes = [];
    $scope.ruledBy = undefined;

    $scope.fetch = function(locationId) {
        // get the location
        api.getLocation(locationId).then(function(data) {
            $scope.location = data;
        }, function(error) {
            console.error(error);
        });
        // get the figures
        api.getFiguresFrom({locationId: locationId}).then(function(data) {
            $scope.figures = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });
        // get the episodes
        api.getEpisodesFrom({locationId: locationId}).then(function(data) {
            console.log(data);
            $scope.episodes = data;
        }, function(error) {
            console.error(error);
        });
        // get the house which rules this location currently
        api.getHousesFrom({locationId: locationId}).then(function(data) {
            $scope.ruledBy = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.locationId);

}]);

gotApp.controller("episodeController", ["$scope", "api", "$routeParams", function($scope, api, $routeParams) {

    $scope.episode = undefined;
    $scope.figures = [];
    $scope.locations = [];

    $scope.fetch = function(episodeId) {
        // get the episode
        api.getEpisode(episodeId).then(function(data) {
            console.log(data);
            $scope.episode = data;
        }, function(error) {
            console.error(error);
        });
        // get the locations in this episode
        api.getLocationsFromEpisode(episodeId).then(function(data) {
            $scope.locations = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });
        // get the figures in this episode
        api.getFiguresFromEpisode(episodeId).then(function(data) {
            $scope.figures = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });
    };

    $scope.fetch($routeParams.episodeId);

}]);

gotApp.controller("homeController", ["$scope", "api", function($scope, api) {

    $scope.figures = [{id: 1, name: "hello"}];
    $scope.houses = [];
    $scope.playlists = [];
    $scope.seasons = [];

    $scope.searchConfig = {
        title: "",
        keyword: undefined,
        section: "",
        prefix: "",
        identifier: "",
        displayId: ""
    };
    $scope.searchMode = false;
    $scope.searchResults = [];

    $scope.alertText = "";
    $scope.alertType = "danger";

    $scope.getAllFigures = function () {
        $scope.searchMode = true;
        $scope.searchConfig.title = "All figures";
        $scope.searchConfig.section = "Figures";
        $scope.searchConfig.prefix = "figures";
        $scope.searchConfig.identifier = "figureId";
        $scope.searchConfig.displayId = "name";
        $scope.searchConfig.keyword = undefined;

        api.searchFigure(undefined).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.log(error);
        });
    };

    $scope.searchFigure = function(keyword) {
        $scope.searchMode = true;
        $scope.searchConfig.title = "Figure Search";
        $scope.searchConfig.keyword = keyword;
        $scope.searchConfig.section = "Figures";
        $scope.searchConfig.prefix = "figures";
        $scope.searchConfig.identifier = "figureId";
        $scope.searchConfig.displayId = "name";

        api.searchFigure(keyword).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.log(error);
        });
    };

    $scope.getAllHouses = function() {
        $scope.searchMode = true;
        $scope.searchConfig.title = "All houses";
        $scope.searchConfig.section = "Houses";
        $scope.searchConfig.prefix = "houses";
        $scope.searchConfig.identifier = "houseId";
        $scope.searchConfig.displayId = "name";
        $scope.searchConfig.keyword = undefined;

        api.searchHouse(undefined).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.searchHouse = function(keyword) {
        $scope.searchMode = true;
        $scope.searchConfig.title = "House Search";
        $scope.searchConfig.keyword = keyword;
        $scope.searchConfig.section = "Houses";
        $scope.searchConfig.prefix = "houses";
        $scope.searchConfig.identifier = "houseId";
        $scope.searchConfig.displayId = "name";

        api.searchHouse(keyword).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.getAllSeasons = function() {
        $scope.searchMode = true;
        $scope.searchConfig.title = "All seasons";
        $scope.searchConfig.section = "Seasons";
        $scope.searchConfig.prefix = "seasons";
        $scope.searchConfig.identifier = "seasonId";
        $scope.searchConfig.displayId = "name";
        $scope.searchConfig.keyword = undefined;

        api.searchSeason(undefined).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.searchSeason = function(keyword) {
        $scope.searchMode = true;
        $scope.searchConfig.title = "Season Search";
        $scope.searchConfig.keyword = keyword;
        $scope.searchConfig.section = "Seasons";
        $scope.searchConfig.prefix = "seasons";
        $scope.searchConfig.identifier = "seasonId";
        $scope.searchConfig.displayId = "name";

        api.searchSeason(keyword).then(function(data) {
            $scope.searchResults = data;
        }, function(error) {
            console.error(error);
        });
    };

    $scope.getAllPlaylists = function () {
        $scope.searchConfig.identifier = "playlistId";
        $scope.searchConfig.displayId = "title";
        $scope.searchConfig.title = "All playlists";
        $scope.searchConfig.section = "Playlists";
        $scope.searchConfig.keyword = undefined;
        $scope.searchConfig.prefix = "playlists";
        $scope.searchMode = true;

        api.getPlaylists(undefined, true).then(function(data) {
            $scope.searchResults = data;
            console.log(data);
        }, function (error) {
            console.error(error);
        });
    };

    $scope.createPlaylist = function (keyword) {
        api.createPlaylist(keyword).then(function(data) {
            $scope.init();
        }, function (error) {
            alert("There was an error when creating the playlist" + error);
        });
    };

    $scope.closeSearchMode = function() {
        $scope.searchMode = false;
    };

    /**
     * Initialize the data
     */
    $scope.init = function() {

        api.getFigures(5).then(function(data) {
            console.log(data);
            $scope.figures = data;
        }, function(error) {
            console.error(error);
        });

        api.getHouses(5).then(function(data) {
            $scope.houses = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });

        api.getSeasons(5).then(function(data) {
            $scope.seasons = data;
            console.log(data);
        }, function(error) {
            console.error(error);
        });

        api.getPlaylists(5, true).then(function(data) {
            $scope.playlists = data;
            console.log(data);
        }, function (error) {
            console.error(error);
        });

    };

    $scope.init();

}]);
