var alerts = angular.module("alerts", []);


alerts.directive("alert", [function() {
    return {
        restrict: "AE",
        templateUrl: "/assets/extra/alert.html",
        scope: {
            type: "=",
            description: "="
        }
    };
}]);
