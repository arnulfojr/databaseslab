var listingDirectives = angular.module("listingDirectives", []);

/**
 * Action panel directive for the entities
 */
listingDirectives.directive("actionPanel", [function() {

    var link = function(scope, element, attributes) {
        scope.keyword = "";
        scope.identifier = (scope.identifier == undefined) ? "id" : scope.identifier;
        scope.displayId = (scope.displayId == undefined) ? "name" : scope.displayId;
        /**
         * Calls the action passed with the keyword
         */
        scope.doAction = function () {
            scope.action({keyword: scope.keyword});
        };

        /**
         * Calls the action passed to show all
         */
        scope.doAll = function() {
            scope.allAction();
        };

        var heading = $(element).find(".panel-heading");

        $(heading).on("click", function() {
            var body = $(this).next();
            var chevron = $(this).children();
            (body.hasClass("hidden")) ? body.removeClass("hidden") : body.addClass("hidden") ;
            if (chevron.hasClass("fa-window-close")) {
                chevron.removeClass("fa-window-close");
                chevron.addClass(scope.iconAction);
            } else {
                chevron.removeClass(scope.iconAction);
                chevron.addClass("fa-window-close");
            }
        });

    };

    return {
        link: link,
        restrict: "EA",
        templateUrl: "assets/extra/actionPanel.html",
        scope: {
            elements: "=",
            action: "&?",
            allAction: "&?",
            title: "@",
            prefix: "@",
            buttonName: "@",
            placeholder: "@",
            labelName: "@",
            identifier: "@?",
            displayId: "@?",
            icon: "@",
            iconAction: "@",
            searchMode: "="
        }
    };
}]);

listingDirectives.directive("searchResults", [function() {

    var link = function(scope, element, attributes) {

        scope.$watch("config", function(n, o) {
            console.log(n);
        });

    };

    return {
        link: link,
        scope: {
            elements: "=",
            config: "="
        },
        restrict: "EA",
        templateUrl: "assets/extra/searchResults.html"
    };
}]);
