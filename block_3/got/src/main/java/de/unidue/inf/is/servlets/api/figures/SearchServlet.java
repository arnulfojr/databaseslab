package de.unidue.inf.is.servlets.api.figures;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.Figure;
import de.unidue.inf.is.stores.FigureStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Search Servlet for the Figures
 */
public class SearchServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String keyword = request.getParameter("keyword");
        FigureStore figureStore = new FigureStore();
        List<Figure> figures = figureStore.search(keyword);

        Gson gson = new Gson();
        String payload = gson.toJson(figures);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }
}
