package de.unidue.inf.is.servlets.security;


import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.stores.UserStore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Log in handler
 */
public class LogInServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        if (request.getAttribute("message") == null)
        {
            request.setAttribute("message", "");
        }

        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");

        if (username != null && !username.isEmpty())
        {
            response.sendRedirect("/home");
            return;
        }
        request.getRequestDispatcher("/templates/security/authentication/login.html.ftl")
                .forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        try
        {
            UserStore userStore = new UserStore();
            User user = userStore.findForAuthentication(username);
            if (user.getUsername() != null && user.getPassword().equals(password))
            {
                //ok
                HttpSession session = request.getSession(true);
                session.setAttribute("username", user.getUsername());
                response.sendRedirect("/home");
                return;
            }

            userStore.close();
            request.setAttribute("message", "Login failed because the credentials were invalid.");
            doGet(request, response);
        }
        catch (StoreException e)
        {
            request.setAttribute("message", e.getMessage());
            doGet(request, response);
        }
    }

}
