package de.unidue.inf.is.domain.playlist;

import de.unidue.inf.is.domain.Storable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Playlist Entity
 */
public class Playlist implements Storable
{
    public static final String TABLE = "PLAYLIST";
    private String title;
    private int playlistId;
    private int userId;
    private List<Item> items;

    public Playlist(String title, int userId)
    {
        this.title = title;
        this.userId = userId;
    }

    public Playlist(ResultSet set)
            throws SQLException
    {
        this.title = set.getString("title");
        this.playlistId = set.getInt("playlistId");
        this.userId = set.getInt("userId");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toInsertStatement()
    {
        return "INSERT INTO "
                + Playlist.TABLE
                + " (playlistId, userId, title)"
                + " VALUES ( DEFAULT , ? , ?)";
    }

    public String toSelectStatement()
    {
        return "SELECT * FROM " + Playlist.TABLE;
    }
}
