package de.unidue.inf.is.domain;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class User implements Storable
{

    public final String TABLE = "USERS";

    private int id;

    private String firstname;

    private String username;

    private String lastname;

    private String password;

    private Timestamp registeredIn;

    public User() {
    }

    public User(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;

    }

    public User(HttpServletRequest request)
    {
        this.firstname = request.getParameter("firstname");
        this.lastname = request.getParameter("lastname");
        this.password = request.getParameter("password");
        this.username = request.getParameter("username");
    }

    public User(ResultSet set)
            throws SQLException
    {
        if (!set.next()) return;
        this.firstname = set.getString("firstname");
        this.lastname = set.getString("lastname");
        this.id = set.getInt("userId");
        this.username = set.getString("username");
        this.password = set.getString("password");
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getRegisteredIn() {
        return registeredIn;
    }

    public void setRegisteredIn(Timestamp registeredIn) {
        this.registeredIn = registeredIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String toInsertStatement()
    {
        return "INSERT INTO "
                + TABLE
                + " (firstname, lastname, username, password, userId, registeredIn)"
                + " VALUES (?, ?, ?, ?, DEFAULT, DEFAULT)";
    }

    public String toSelectStatement()
    {
        return "SELECT firstname, lastname, password, userId, registeredIn FROM"
                + TABLE + ";";
    }

}