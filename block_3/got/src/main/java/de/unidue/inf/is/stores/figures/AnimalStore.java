package de.unidue.inf.is.stores.figures;

import de.unidue.inf.is.domain.figures.Animal;
import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.LocationStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Animal Store
 */
public class AnimalStore extends BaseStore
{
    public AnimalStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Finds an animal by its Id
     */
    public Animal find(int animalId)
    {
        try
        {
            String statement = Animal.toSelectStatement();
            statement += " WHERE animals.animalId = ? ";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, animalId);
            ResultSet resultSet = preparedStatement.executeQuery();

            Animal animal = null;

            if (resultSet.next())
            {
                animal = new Animal(resultSet);
                animal = getObject(animal, true);
            }

            resultSet.close();
            preparedStatement.close();

            return animal;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Animal> findPetsFrom(Person person)
    {
        return findPetsFrom(person.getPersonId(), false);
    }

    public List<Animal> findPetsFrom(int personId, boolean includeOwner)
    {
        try
        {
            String statement = Animal.toSelectStatement();
            statement += " WHERE animals.ownerId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, personId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Animal> animals = getObjects(resultSet, includeOwner);
            preparedStatement.close();

            return animals;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns the animal with the given Figure Id
     */
    public Animal findByFigureId(int figureId)
    {
        try
        {
            String statement = Animal.toSelectStatement();
            statement += " WHERE animals.figureId = ? ";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, figureId);
            ResultSet resultSet = preparedStatement.executeQuery();

            Animal animal = null;

            if (resultSet.next())
            {
                animal = new Animal(resultSet);
                animal = getObject(animal, true);
            }

            resultSet.close();
            preparedStatement.close();

            return animal;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Gets a list of fully instantiated Animals
     */
    private List<Animal> getObjects(ResultSet resultSet, boolean includeOwner)
            throws SQLException
    {

        List<Animal> animals = new ArrayList<>();

        while (resultSet.next())
        {
            Animal animal = new Animal(resultSet);
            animals.add(getObject(animal, includeOwner));
        }

        resultSet.close();

        return animals;
    }

    /**
     * Returns a full instantiated object
     */
    private Animal getObject(Animal animal, boolean includeOwner)
            throws SQLException
    {
        LocationStore locationStore = new LocationStore();

        animal.setOrigin(locationStore.find(animal.getOriginId()));

        locationStore.commit();
        locationStore.close();

        if (includeOwner)
        {
            PersonStore personStore = new PersonStore();
            animal.setOwner(personStore.find(animal.getOwnerId(), false));

            personStore.commit();
            personStore.close();
        }

        return animal;
    }

}
