package de.unidue.inf.is.stores;


import de.unidue.inf.is.domain.Location;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Location Store
 */
public class LocationStore extends BaseStore
{

    public LocationStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Override of the constructor if an already given connection was established
     */
    public LocationStore(Connection connection)
    {
        this.connection = connection;
    }

    public Location find(int locationId)
    {
        Location location;
        try
        {
            String stmt = "SELECT * FROM " + Location.TABLE + " WHERE locationId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, locationId);
            ResultSet set = pStmt.executeQuery();

            if (!set.next())
            {
                set.close();
                pStmt.close();

                return null;
            }

            location = new Location(set);
            pStmt.close();

            return location;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Find all the locations with a limit
     */
    public List<Location> findAll(int limit)
    {
        limit = (limit < 0) ? 9999 : limit;
        PreparedStatement preparedStatement;
        try
        {
            String stmt = "SELECT * FROM " + Location.TABLE;
            preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setMaxRows(limit);
            ResultSet set = preparedStatement.executeQuery();
            List<Location> locations = getListFrom(set);

            preparedStatement.close();

            return locations;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns a list of Locations based on a result Set
     */
    private List<Location> getListFrom(ResultSet resultSet)
            throws SQLException
    {
        List<Location> locations = new ArrayList<>();

        while (resultSet.next())
        {
            locations.add(new Location(resultSet));
        }

        resultSet.close();
        return locations;
    }

    public List<Location> findFromEpisode(int episodeId)
    {
        try
        {
            String stmt = "SELECT " + Location.TABLE + ".locationId, NAME"
                    + " FROM " + Location.TABLE
                    + " INNER JOIN "
                    + Location.EPISODE_RELATED_TABLE
                    + " ON "
                    + Location.TABLE + ".locationId = "
                    + Location.EPISODE_RELATED_TABLE + ".locationId"
                    + " WHERE " + Location.EPISODE_RELATED_TABLE + ".episodeId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, episodeId);
            ResultSet set = pStmt.executeQuery();

            List<Location> locations = getListFrom(set);
            pStmt.close();

            return locations;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

}
