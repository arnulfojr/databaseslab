package de.unidue.inf.is.stores;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


public class BaseStore implements Closeable, StoreInterface
{
    protected boolean commited = false;
    protected Connection connection;

    /**
     * Sets the connection of the Store
     */
    protected void setConnection(Connection connection)
    {
        this.connection = connection;
    }

    @Override
    public void commit()
    {
        this.commited = true;
    }

    @Override
    public void close()
    {
        if (connection == null) return; // nothing to close
        try
        {
            if (this.commited) this.connection.commit();
            else this.connection.rollback();
            this.connection.close();
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }
}
