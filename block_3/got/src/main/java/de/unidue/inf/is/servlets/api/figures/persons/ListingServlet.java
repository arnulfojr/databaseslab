package de.unidue.inf.is.servlets.api.figures.persons;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.stores.figures.PersonStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Persons listing servlet
 */
public class ListingServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String personIdString = request.getParameter("personId");
        int personId = (personIdString == null || personIdString.isEmpty()) ? -1 : Integer.parseInt(personIdString);

        Gson gson = new Gson();
        String payload = "";

        if (personId > -1)
        {
            // return the person
            PersonStore personStore = new PersonStore();
            Person person = personStore.find(personId, true, true);
            payload = gson.toJson(person);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(payload);
    }
}
