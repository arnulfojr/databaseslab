package de.unidue.inf.is.stores.film;

import de.unidue.inf.is.domain.film.Episode;
import de.unidue.inf.is.domain.film.Season;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Season Store
 */
public class SeasonStore extends BaseStore
{
    public SeasonStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns a list of Seasons
     */
    public List<Season> findAll()
    {
        try
        {
            String stmt = "SELECT * FROM " + Season.TABLE;
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            List<Season> seasons = getList(pStmt);

            pStmt.close();

            return seasons;
        }catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Searches for a matching Season with the keyword
     */
    public List<Season> search(String keyword)
    {
        try
        {
            if (keyword == null || keyword.isEmpty()) return findAll();
            String stmt = "SELECT * FROM " + Season.TABLE
                    + " WHERE name LIKE ? ";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setString(1, "%" + keyword + "%");
            List<Season> seasons = getList(preparedStatement);

            preparedStatement.close();

            return seasons;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Executes the Query from the PreparedStatement and returns a List of Seasons
     */
    private List<Season> getList(PreparedStatement preparedStatement)
            throws SQLException
    {
        ResultSet set = preparedStatement.executeQuery();
        return getList(set);
    }

    /**
     * Returns a list of Seasons based on the Result Set passed
     */
    private List<Season> getList(ResultSet resultSet)
        throws SQLException
    {
        List<Season> seasons = new ArrayList<>();
        EpisodeStore episodeStore = new EpisodeStore();

        while(resultSet.next())
        {
            Season currentSeason = new Season(resultSet);
            List<Episode> episodes = episodeStore.findBySeason(currentSeason.getSeasonId());
            currentSeason.setEpisodes(episodes);
            seasons.add(currentSeason);
        }

        episodeStore.commit();
        resultSet.close();

        return seasons;
    }

    public Season find(int seasonId)
    {
        try
        {
            String stmt = "SELECT * FROM " + Season.TABLE + " WHERE seasonId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, seasonId);
            ResultSet set = pStmt.executeQuery();

            if (!set.next())
            {
                set.close();
                pStmt.close();
                return null;
            }

            Season season = new Season(set);

            set.close();
            pStmt.close();

            EpisodeStore episodeStore = new EpisodeStore();
            List<Episode> episodes = episodeStore.findBySeason(season.getSeasonId());
            episodeStore.commit();
            season.setEpisodes(episodes);

            return season;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Season get(Episode episode)
    {
        try
        {
            String stmt = "SELECT * FROM " + Season.TABLE + " WHERE seasonId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, episode.getSeasonId());
            ResultSet set = pStmt.executeQuery();
            if (!set.next()) return null;
            Season season = new Season(set);

            set.close();
            pStmt.close();

            return season;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Season find(Episode episode)
    {
        return find(episode.getSeasonId());
    }

}
