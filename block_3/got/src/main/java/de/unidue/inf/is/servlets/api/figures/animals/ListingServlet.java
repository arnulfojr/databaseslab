package de.unidue.inf.is.servlets.api.figures.animals;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.figures.Animal;
import de.unidue.inf.is.stores.figures.AnimalStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Listing servlet for Animals
 */
public class ListingServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String animalIdString = request.getParameter("animalId");
        int animalId = (animalIdString == null || animalIdString.isEmpty()) ? -1 : Integer.parseInt(animalIdString);
        String ownerIdString = request.getParameter("ownerId");
        int ownerId = (ownerIdString == null || ownerIdString.isEmpty()) ? -1 : Integer.parseInt(ownerIdString);

        String payload = "";
        Gson gson = new Gson();

        if (animalId > -1)
        {
            AnimalStore animalStore = new AnimalStore();
            Animal animal = animalStore.find(animalId);

            animalStore.commit();
            animalStore.close();

            payload = gson.toJson(animal);
        } else {
            if (ownerId > -1)
            {
                // get the pets from the owner
                AnimalStore animalStore = new AnimalStore();
                List<Animal> animals = animalStore.findPetsFrom(ownerId, true);
                animalStore.commit();
                animalStore.close();

                payload = gson.toJson(animals);
            }
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(payload);
    }
}
