package de.unidue.inf.is.servlets.api.locations;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.House;
import de.unidue.inf.is.domain.Location;
import de.unidue.inf.is.domain.Ruled;
import de.unidue.inf.is.stores.HouseStore;
import de.unidue.inf.is.stores.LocationStore;
import de.unidue.inf.is.stores.RuledStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Ruled Locations servlet
 */
public class RuledLocationsServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String houseIdString = request.getParameter("houseId");
        int houseId = (houseIdString == null || houseIdString.isEmpty()) ? -1 : Integer.parseInt(houseIdString);

        Gson gson = new Gson();
        String payload = "";

        if (houseId > -1)
        {
            RuledStore ruledStore = new RuledStore();
            List<Ruled> ruledList = ruledStore.findByHouseId(houseId);
            payload = gson.toJson(ruledList);
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }
}
