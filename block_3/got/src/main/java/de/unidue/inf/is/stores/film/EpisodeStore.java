package de.unidue.inf.is.stores.film;

import de.unidue.inf.is.domain.film.Episode;
import de.unidue.inf.is.domain.film.Season;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Episode Store
 */
public class EpisodeStore extends BaseStore
{
    public EpisodeStore() throws StoreException
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Episode find(int episodeId)
    {
        try
        {
            String stmt = "SELECT * FROM " + Episode.TABLE + " WHERE episodeId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, episodeId);
            ResultSet resultSet = pStmt.executeQuery();
            if (!resultSet.next())
            {
                resultSet.close();
                pStmt.close();
                return null;
            }
            Episode episode = new Episode(resultSet);

            // get the season the episode it belongs to
            SeasonStore seasonStore = new SeasonStore();
            Season season = seasonStore.find(episode);
            seasonStore.commit();
            episode.setSeason(season);

            resultSet.close();
            pStmt.close();

            return episode;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Episode> findAll()
    {
        try
        {
            String stmt = "SELECT * FROM " + Episode.TABLE;
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            List<Episode> episodes = getList(preparedStatement);
            preparedStatement.close();

            return episodes;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Episode> findBySeason(int seasonId)
    {
        try {
            String stmt = "SELECT * FROM " + Episode.TABLE + " WHERE seasonId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, seasonId);
            List<Episode> episodes = getList(pStmt);

            pStmt.close();

            return episodes;
        } catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Episode> findByLocation(int locationId)
    {
        try
        {
            String statement = "SELECT ep.* FROM " + Episode.TABLE + " ep";
            statement += " INNER JOIN " + Episode.LOCATION_RELATED_TABLE + " loc";
            statement += " ON ep.episodeId = loc.episodeId";
            statement += " WHERE loc.locationId = ? ";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, locationId);
            List<Episode> episodes = getList(preparedStatement, true);
            preparedStatement.close();
            return episodes;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    private List<Episode> getList(PreparedStatement statement)
    {
        return getList(statement, false);
    }

    private List<Episode> getList(PreparedStatement statement, boolean includeSeasons)
    {
        try
        {
            List<Episode> episodes = new ArrayList<>();
            ResultSet set = statement.executeQuery();

            while (set.next())
            {
                Episode episode = new Episode(set);
                if (includeSeasons)
                {
                    SeasonStore store = new SeasonStore();
                    episode.setSeason(store.get(episode));
                    store.commit();
                    store.close();
                }
                episodes.add(episode);
            }

            set.close();
            return episodes;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

}
