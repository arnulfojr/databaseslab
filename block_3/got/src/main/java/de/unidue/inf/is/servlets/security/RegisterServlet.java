package de.unidue.inf.is.servlets.security;


import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.stores.UserStore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class is in charge of rendering the Register Form and handling the registration
 */
public class RegisterServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        // simply serve the registration view
        request.getRequestDispatcher("/templates/security/registration/register.html.ftl")
                .forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        User newUser = new User(request);
        try
        {
            UserStore userStore = new UserStore();
            userStore.addUser(newUser);
            // redirect to login!
            userStore.commit();
            userStore.close();
            response.sendRedirect("/login");
        }
        catch (StoreException e)
        {
            response.sendError(500, e.getMessage());
        }
    }

}
