package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.Castle;
import de.unidue.inf.is.domain.House;
import de.unidue.inf.is.domain.film.Episode;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * House Store / Repository
 */
public class HouseStore extends BaseStore
{
    public HouseStore()
            throws StoreException
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        } catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns the result of the search based on the keyword
     */
    public List<House> search(String keyword)
    {
        try
        {
            if (keyword == null || keyword.isEmpty()) return findAll(1000);
            String statement = "SELECT * FROM " + House.TABLE
                    + " WHERE name LIKE ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setString(1, "%" + keyword + "%");
            List<House> houses = getList(preparedStatement);

            preparedStatement.close();

            return houses;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public House find(int houseId)
    {
        return find(houseId, false);
    }

    public House find(int houseId, boolean includeCastle)
    {
        try
        {
            String statement = "SELECT * FROM " + House.TABLE;
            statement += " WHERE houseId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, houseId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (!resultSet.next())
            {
                resultSet.close();
                preparedStatement.close();
                return null;
            }

            House house = new House(resultSet);

            resultSet.close();
            preparedStatement.close();

            if (!includeCastle) return house;

            CastleStore castleStore = new CastleStore();
            Castle castle = castleStore.find(house.getSeatsInId());
            house.setCastle(castle);

            return house;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public House searchLatestRuled(int locationId)
    {
        try
        {
            String statement = "SELECT * FROM " + House.TABLE + " house";
            statement += " INNER JOIN " + House.RULED_TABLED + " rloc";
            statement += " ON house.HOUSEID = rloc.HOUSEID";
            statement += " INNER JOIN " + Episode.TABLE + " to_ep";
            statement += " ON rloc.TOEPISODE = to_ep.EPISODEID";
            statement += " WHERE rloc.locationId = ? ";
            statement += " ORDER BY to_ep.NUMBER DESC";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, locationId);
            preparedStatement.setMaxRows(1);
            ResultSet set = preparedStatement.executeQuery();

            if (!set.next())
            {
                set.close();
                preparedStatement.close();
                return null;
            }

            House house = new House(set);
            set.close();
            preparedStatement.close();
            return house;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns a List of houses
     */
    public List<House> findAll(int limit)
    {
        limit = (limit <= 0) ? 1000 : limit;
        try
        {
            String statement = "SELECT * FROM " + House.TABLE;
            PreparedStatement pStmt = this.connection.prepareStatement(statement);
            pStmt.setMaxRows(limit);
            List<House> houses = getList(pStmt);

            pStmt.close();

            return houses;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns a list of Houses based on a PreparedStatement
     */
    private List<House> getList(PreparedStatement preparedStatement)
            throws SQLException
    {
        ResultSet set = preparedStatement.executeQuery();
        List<House> houses = new ArrayList<>();
        while(set.next())
        {
            CastleStore store = new CastleStore();
            House currentHouse = new House(set);
            Castle castle = store.find(currentHouse.getSeatsInId());
            store.commit();
            currentHouse.setCastle(castle);
            houses.add(currentHouse);
        }
        set.close();

        return houses;
    }

}
