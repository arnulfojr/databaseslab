package de.unidue.inf.is.domain;

import de.unidue.inf.is.domain.film.Episode;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Ruled Locations wrapper
 */
public class Ruled
{
    public static final String TABLE = "RULEDLOCATIONS";
    private int houseId;
    private House house;
    private int fromEpisodeId;
    private Episode fromEpisode;
    private int toEpisodeId;
    private Episode toEpisode;
    private int locationId;
    private Location location;

    public Ruled(ResultSet resultSet)
        throws SQLException
    {
        this.houseId = resultSet.getInt("houseId");
        this.fromEpisodeId = resultSet.getInt("fromEpisode");
        this.toEpisodeId = resultSet.getInt("toEpisode");
        this.locationId = resultSet.getInt("locationId");
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public int getFromEpisodeId() {
        return fromEpisodeId;
    }

    public void setFromEpisodeId(int fromEpisodeId) {
        this.fromEpisodeId = fromEpisodeId;
    }

    public Episode getFromEpisode() {
        return fromEpisode;
    }

    public void setFromEpisode(Episode fromEpisode) {
        this.fromEpisode = fromEpisode;
    }

    public int getToEpisodeId() {
        return toEpisodeId;
    }

    public void setToEpisodeId(int toEpisodeId) {
        this.toEpisodeId = toEpisodeId;
    }

    public Episode getToEpisode() {
        return toEpisode;
    }

    public void setToEpisode(Episode toEpisode) {
        this.toEpisode = toEpisode;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
