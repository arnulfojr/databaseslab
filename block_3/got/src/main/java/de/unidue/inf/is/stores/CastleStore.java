package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.Castle;
import de.unidue.inf.is.domain.Location;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Castle Store
 */
public class CastleStore extends BaseStore
{
    /**
     * Constructor to get a new connection from the pool
     */
    public CastleStore()
    {
        try
        {
            connection = ConnectionFactory.getConnection();
            connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Castle find(int castleId)
    {
        try
        {
            String stmt = "SELECT * FROM " + Castle.TABLE + " WHERE castleId = ?";
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, castleId);
            ResultSet resultSet = pStmt.executeQuery();
            Castle castle = new Castle(resultSet);
            LocationStore locationStore = new LocationStore();
            Location location = locationStore.find(castle.getLocationId());
            locationStore.commit();
            castle.setLocation(location);

            resultSet.close();
            pStmt.close();

            return castle;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

}
