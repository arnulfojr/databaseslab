package de.unidue.inf.is.domain.film;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Season entity
 */
public class Season
{
    public static final String TABLE = "SEASONS";

    private int seasonId;
    private Date startDate;
    private String name;
    private int numberOfEpisodes;
    private List<Episode> episodes;

    public Season()
    {
        this.episodes = new ArrayList<>();
    }

    public Season(ResultSet set)
            throws SQLException
    {
        this.episodes = new ArrayList<>();
        this.seasonId = set.getInt("seasonId");
        this.startDate = set.getDate("startDate");
        this.name = set.getString("name");
        this.numberOfEpisodes = set.getInt("numberOfEpisodes");
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    public int getNumberOfEpisodes() {
        return numberOfEpisodes;
    }

    public void setNumberOfEpisodes(int numberOfEpisodes) {
        this.numberOfEpisodes = numberOfEpisodes;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {

        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getSeasonId() {

        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }
}
