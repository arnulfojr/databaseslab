package de.unidue.inf.is.servlets.api.houses;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.House;
import de.unidue.inf.is.stores.HouseStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet for the Listing of Houses
 */
public class ListingServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String limitString = request.getParameter("limit");
        int limit = (limitString != null && !limitString.isEmpty()) ? Integer.parseInt(limitString) : -1;
        String locationIdString = request.getParameter("locationId");
        int locationId = (locationIdString == null || locationIdString.isEmpty()) ? -1 : Integer.parseInt(locationIdString);
        String houseIdString = request.getParameter("houseId");
        int houseId = (houseIdString == null || houseIdString.isEmpty()) ? -1 : Integer.parseInt(houseIdString);

        HouseStore store = new HouseStore();
        Gson gson = new Gson();
        String payload;

        if (houseId > -1)
        {
            // house is being requested
            House house = store.find(houseId, true);
            payload = gson.toJson(house);
        } else {
            if (locationId > -1)
            {
                // a location id was given
                House house = store.searchLatestRuled(locationId);
                payload = gson.toJson(house);
            } else {
                // return all, no criteria
                List<House> houses = store.findAll(limit);
                payload = gson.toJson(houses);
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);

    }
}
