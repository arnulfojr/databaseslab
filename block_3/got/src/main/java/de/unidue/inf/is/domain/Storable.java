package de.unidue.inf.is.domain;

/**
 * Storable means that each domain will return their own sql statement
 */
public interface Storable
{
    String toInsertStatement();
}
