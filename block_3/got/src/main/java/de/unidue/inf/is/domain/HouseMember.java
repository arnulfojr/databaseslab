package de.unidue.inf.is.domain;

import de.unidue.inf.is.domain.film.Episode;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * House Member entity
 */
public class HouseMember
{

    public final static String TABLE = "HOUSEMEMBERS";

    private int houseId;
    private House house;

    private int personId;
    private Figure person;  // can be a person or animal?

    private int fromEpisodeId;
    private Episode fromEpisode;

    private int toEpisodeId;
    private Episode toEpisode;

    public HouseMember() {}

    public HouseMember(ResultSet resultSet)
            throws SQLException
    {
        this.houseId = resultSet.getInt("houseId");
        this.personId = resultSet.getInt("personId");
        this.fromEpisodeId = resultSet.getInt("fromEpisodeId");
        this.toEpisodeId = resultSet.getInt("toEpisodeId");
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public Figure getPerson() {
        return person;
    }

    public void setPerson(Figure person) {
        this.person = person;
    }

    public int getFromEpisodeId() {
        return fromEpisodeId;
    }

    public void setFromEpisodeId(int fromEpisodeId) {
        this.fromEpisodeId = fromEpisodeId;
    }

    public Episode getFromEpisode() {
        return fromEpisode;
    }

    public void setFromEpisode(Episode fromEpisode) {
        this.fromEpisode = fromEpisode;
    }

    public int getToEpisodeId() {
        return toEpisodeId;
    }

    public void setToEpisodeId(int toEpisodeId) {
        this.toEpisodeId = toEpisodeId;
    }

    public Episode getToEpisode() {
        return toEpisode;
    }

    public void setToEpisode(Episode toEpisode) {
        this.toEpisode = toEpisode;
    }

    public static String toSelectStatement()
    {
        return "SELECT members.* FROM " + TABLE + " members";
    }

}
