package de.unidue.inf.is.domain.figures;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Relationship Entity
 */
public class Relationship
{
    public static final String TABLE = "RELATIONSHIP";

    private String type;
    private int fromWhoId;
    private int toWhomId;
    private Person toWhom;

    public Relationship() {}

    public Relationship(ResultSet resultSet)
        throws SQLException
    {
        this.type = resultSet.getString("relationshipName");
        this.toWhomId = resultSet.getInt("targetPersonId");
        this.fromWhoId = resultSet.getInt("sourcePersonId");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getToWhomId() {
        return toWhomId;
    }

    public void setToWhomId(int toWhomId) {
        this.toWhomId = toWhomId;
    }

    public Person getToWhom() {
        return toWhom;
    }

    public void setToWhom(Person toWhom) {
        this.toWhom = toWhom;
    }

    public int getFromWhoId() {
        return fromWhoId;
    }

    public void setFromWhoId(int fromWhoId) {
        this.fromWhoId = fromWhoId;
    }

    public static String toSelectStatement()
    {
        String statement = "SELECT rel.* FROM " + TABLE + " rel";
        return statement;
    }

}
