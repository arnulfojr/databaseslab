package de.unidue.inf.is.stores;

import java.sql.*;

import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.utils.ConnectionFactory;


/**
 * Repository for all users actions
 */
public final class UserStore extends BaseStore
{
    public UserStore()
            throws StoreException
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }


    /**
     * @param userToAdd user domain to persist
     */
    public void addUser(User userToAdd)
            throws StoreException
    {
        try
        {
            PreparedStatement preparedStatement = this.connection
                            .prepareStatement(userToAdd.toInsertStatement());
            preparedStatement.setString(1, userToAdd.getFirstname());
            preparedStatement.setString(2, userToAdd.getLastname());
            preparedStatement.setString(3, userToAdd.getUsername());
            preparedStatement.setString(4, userToAdd.getPassword());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public User findForAuthentication(String username)
    {
        ResultSet resultSet = null;
        try
        {
            String statement = "SELECT * FROM USERS WHERE USERNAME = ?";
            PreparedStatement pStatement = this.connection.prepareStatement(statement);
            pStatement.setString(1, username);
            resultSet = pStatement.executeQuery();
            User user = new User(resultSet);

            resultSet.close();
            pStatement.close();

            return user;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

}
