package de.unidue.inf.is.servlets.api.locations;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.Location;
import de.unidue.inf.is.stores.LocationStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to return the information about the location
 */
public class ListingServlet extends HttpServlet
{

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String episodeIdString = request.getParameter("episodeId");
        int episodeId = (episodeIdString != null && !episodeIdString.isEmpty()) ? Integer.parseInt(episodeIdString) : -1;
        String locationIdString = request.getParameter("locationId");
        int locationId = (locationIdString != null && !locationIdString.isEmpty()) ? Integer.parseInt(locationIdString) : -1;
        String limitString = request.getParameter("limit");
        int limit = (limitString != null && !limitString.isEmpty()) ? Integer.parseInt(limitString) : -1;

        // ok now the request about the location
        LocationStore locationStore = new LocationStore();
        Gson gson = new Gson();
        String payload;

        if (locationId > -1)
        {
            // get the location based on the locationId
            Location location = locationStore.find(locationId);
            payload = gson.toJson(location);
        } else if (episodeId > -1) {
            List<Location> locations = locationStore.findFromEpisode(episodeId);
            payload = gson.toJson(locations);
        } else {
            // get a list of all locations
            List<Location> locations = locationStore.findAll(limit);
            payload = gson.toJson(locations);  // just get an empty list
        }

        locationStore.close();
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().write(payload);
    }

}
