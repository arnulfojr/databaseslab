package de.unidue.inf.is.servlets.api.houses;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.HouseMember;
import de.unidue.inf.is.stores.MemberStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Members Servlet
 */
public class MembersServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String houseIdString = request.getParameter("houseId");
        int houseId = (houseIdString == null || houseIdString.isEmpty()) ? -1 : Integer.parseInt(houseIdString);
        String personIdString = request.getParameter("personId");
        int personId = (personIdString == null || personIdString.isEmpty()) ? -1 : Integer.parseInt(personIdString);

        String payload = "";
        Gson gson = new Gson();

        if (houseId > -1)
        {
            MemberStore memberStore = new MemberStore();
            List<HouseMember> members = memberStore.findMembersFrom(houseId);
            payload = gson.toJson(members);
            memberStore.close();
        } else {
            if (personId > -1)
            {
                MemberStore memberStore = new MemberStore();
                List<HouseMember> memberships = memberStore.findMembershipsFrom(personId);
                payload = gson.toJson(memberships);
                memberStore.close();
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }
}
