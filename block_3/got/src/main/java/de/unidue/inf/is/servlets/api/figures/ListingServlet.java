package de.unidue.inf.is.servlets.api.figures;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.Figure;
import de.unidue.inf.is.stores.FigureStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Api call to get the Figures
 */
public class ListingServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String limitString = request.getParameter("limit");
        int limit = (limitString == null || limitString.isEmpty()) ? 0 : Integer.parseInt(limitString);
        String episodeIdString = request.getParameter("episodeId");
        int episodeId = (episodeIdString == null || episodeIdString.isEmpty()) ? -1 : Integer.parseInt(episodeIdString);
        String locationIdString = request.getParameter("locationId");
        int locationId = (locationIdString == null || locationIdString.isEmpty()) ? -1 : Integer.parseInt(locationIdString);

        FigureStore store = new FigureStore();
        String responseBody;
        Gson gson = new Gson();

        if (episodeId >= 0)
        {
            // then a valid episodeId is being requested
            List<Figure> figures = store.findByEpisodeId(episodeId);
            responseBody = gson.toJson(figures);
        } else if (locationId >= 0) {
            List<Figure> figures = store.findByOriginId(locationId);
            responseBody = gson.toJson(figures);
        } else {
            List<Figure> figures = store.findAll(limit);
            responseBody = gson.toJson(figures);
        }
        store.commit();

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(responseBody);
    }
}
