package de.unidue.inf.is.servlets.api.figures;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.Figure;
import de.unidue.inf.is.stores.FigureStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Specifier Servlet gets a Figure Id and attempts to find the correct Entity
 */
public class SpecifierServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");

        String specificIdString = request.getParameter("specificId");

        if (specificIdString == null || specificIdString.isEmpty())
        {
            // return nothing if there was no specific id
            response.getWriter().write("[]");
            return;
        }

        int specificId = Integer.parseInt(specificIdString);

        Gson gson = new Gson();

        FigureStore figureStore = new FigureStore();
        Figure figure = figureStore.guess(specificId);

        String payload = gson.toJson(figure);
        response.getWriter().write(payload);

    }
}
