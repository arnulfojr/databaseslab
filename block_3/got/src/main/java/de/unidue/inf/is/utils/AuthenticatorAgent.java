package de.unidue.inf.is.utils;

import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.UserStore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Authenticator agent to check if the request and response are valid
 * This is based on the username
 */
public class AuthenticatorAgent
{
    /**
     * Returns a boolean value on the condition of the User being logged in or not
     */
    public static boolean isLogged(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");

        return (username != null && !username.isEmpty() && !username.equals(""));
    }

    /**
     * Returns a valid User or null
     */
    public static User getUser(HttpServletRequest request)
    {
        if (!AuthenticatorAgent.isLogged(request)) return null;
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");

        UserStore userStore = new UserStore();
        User user = userStore.findForAuthentication(username);

        if (user == null || user.getUsername() == null) return null;
        return user;
    }

}
