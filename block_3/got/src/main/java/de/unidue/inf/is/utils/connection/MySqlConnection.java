package de.unidue.inf.is.utils.connection;

import com.mysql.cj.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * MySQL connection wrapper
 */
public class MySqlConnection implements ConnectionWrapperInterface
{
    @Override
    public void registerDriver()
    {
        try
        {
            Driver driver = new Driver();
            DriverManager.registerDriver(driver);
        }
        catch (SQLException e)
        {
            throw new Error("MySQL Driver couldn't be loaded");
        }
    }

    @Override
    public Connection getConnection()
            throws SQLException
    {
        return this.getConnection("database");
    }

    @Override
    public Connection getConnection(String database)
            throws SQLException
    {
        Properties properties = new Properties();
        properties.setProperty("user", "gotuser");
        properties.setProperty("password", "gotpassword");
        properties.setProperty("verifyServerCertificate", "false");
        properties.setProperty("useSSL", "false");
        String url = "jdbc:mysql://0.0.0.0:50001/" + database;
        return DriverManager.getConnection(url, properties);
    }

}
