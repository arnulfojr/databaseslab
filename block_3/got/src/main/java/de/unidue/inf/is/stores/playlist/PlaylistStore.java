package de.unidue.inf.is.stores.playlist;

import de.unidue.inf.is.domain.playlist.Item;
import de.unidue.inf.is.domain.playlist.Playlist;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Playlist store
 */
public class PlaylistStore extends BaseStore
{
    public PlaylistStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns the full playlists of the user
     */
    public List<Playlist> findAll(int userId, int limit)
    {
        return findAll(userId, false, limit);
    }

    /**
     * This shall return a list of playlist for catalog,
     * meaning only the playlists info not the items
     */
    public List<Playlist> findAll(int userId, boolean catalog, int limit)
    {
        try {
            limit = (limit < 1) ? 1000 : limit;
            // query to get the user's playlist
            String stmt = "SELECT * FROM " + Playlist.TABLE + " WHERE userId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setInt(1, userId);
            preparedStatement.setMaxRows(limit);

            List<Playlist> playlists = getList(preparedStatement, catalog);

            preparedStatement.close();

            return playlists;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Creates a playlist with the given title and userId
     */
    public Playlist create(String title, int userId)
    {
        try
        {
            // create an instance of Playlist and return it
            Playlist playlist = new Playlist(title, userId);
            String stmt = playlist.toInsertStatement();
            PreparedStatement pStmt = this.connection.prepareStatement(stmt);
            pStmt.setInt(1, userId);
            pStmt.setString(2, title);
            int rowCount = pStmt.executeUpdate();

            pStmt.close();

            return playlist;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * returns the playlist for the given Id (only the entity)
     */
    public Playlist find(int playlistId)
    {
        return find(playlistId, false);
    }

    /**
     * returns the full contents of the playlist
     */
    public Playlist find(int playlistId, boolean catalog)
    {
        try
        {
            String stmt = "SELECT * FROM " + Playlist.TABLE
                    + " WHERE playlistId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setInt(1, playlistId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next())
            {
                Playlist playlist = new Playlist(resultSet);
                resultSet.close();
                preparedStatement.close();

                if (catalog) return playlist;

                ItemStore itemStore = new ItemStore();
                List<Item> items = itemStore.findAll(playlist.getPlaylistId());
                playlist.setItems(items);
                return playlist;
            }

            resultSet.close();
            preparedStatement.close();

            return null;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Searches for a playlist with the title
     */
    public List<Playlist> search(String keyword, boolean catalog)
    {
        try
        {
            String statement = "SELECT * FROM " + Playlist.TABLE + " WHERE title LIKE ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setString(1, "%" + keyword + "%");

            List<Playlist> playlists = getList(preparedStatement, catalog);

            preparedStatement.close();

            return playlists;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    private List<Playlist> getList(PreparedStatement preparedStatement, boolean catalog)
            throws SQLException
    {
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Playlist> playlists = new ArrayList<>();

        while (resultSet.next())
        {
            Playlist playlist = new Playlist(resultSet);
            playlists.add(playlist);
            if (catalog) continue;
            // add all the items into the playlist
            ItemStore itemStore = new ItemStore();
            List<Item> items = itemStore.findAll(playlist.getPlaylistId());
            playlist.setItems(items);
        }

        resultSet.close();

        return playlists;
    }

}
