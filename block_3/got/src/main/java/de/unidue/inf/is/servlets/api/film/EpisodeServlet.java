package de.unidue.inf.is.servlets.api.film;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.film.Episode;
import de.unidue.inf.is.stores.film.EpisodeStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Episode Servlet
 */
public class EpisodeServlet extends HttpServlet
{
    /**
     * If seasonId is set then it returns all the episodes from the season
     * If the episodeId is set then it returns the info for the episode
     * If it is not set, then returns all episodes!
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String seasonIdString = request.getParameter("seasonId");
        String episodeIdString = request.getParameter("episodeId");
        String locationString = request.getParameter("locationId");
        int seasonId = (seasonIdString == null) ? -1 : Integer.parseInt(seasonIdString);
        int episodeId = (episodeIdString == null) ? -1 : Integer.parseInt(episodeIdString);
        int locationId = (locationString == null) ? -1 : Integer.parseInt(locationString);

        EpisodeStore episodeStore = new EpisodeStore();
        String payload;
        Gson gson = new Gson();

        if (seasonId == -1)
        {
            if (episodeId == -1)
            {
                if (locationId == -1)
                {
                    // return all episodes
                    List<Episode> episodes = episodeStore.findAll();
                    payload = gson.toJson(episodes);
                } else {
                    // return the episodes based on the location id
                    List<Episode> episodes = episodeStore.findByLocation(locationId);
                    payload = gson.toJson(episodes);
                }
            } else {
                // return details of the episode
                Episode episode = episodeStore.find(episodeId);
                payload = gson.toJson(episode);
            }
        } else {
            // return the episodes from the season
            List<Episode> episodes = episodeStore.findBySeason(seasonId);
            payload = gson.toJson(episodes);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");

        response.getWriter().write(payload);
    }
}
