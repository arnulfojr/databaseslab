package de.unidue.inf.is.servlets.api.film;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.film.Season;
import de.unidue.inf.is.stores.film.SeasonStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Season Search Servlet
 */
public class SeasonSearchServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String keyword = request.getParameter("keyword");
        SeasonStore seasonStore = new SeasonStore();
        List<Season> seasons = seasonStore.search(keyword);

        Gson gson = new Gson();
        String payload = gson.toJson(seasons);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }
}
