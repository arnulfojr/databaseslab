package de.unidue.inf.is.domain.figures;

import de.unidue.inf.is.domain.Figure;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Animal Entity
 */
public class Animal extends Figure
{
    public static final String TABLE = "ANIMALS";

    private int animalId;

    private int ownerId;
    private Person owner;

    public Animal()
    {
        super();
    }

    public Animal(ResultSet resultSet)
        throws SQLException
    {
        super(resultSet);
        this.animalId = resultSet.getInt("animalId");
        this.ownerId = resultSet.getInt("ownerId");
    }

    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }


    public static String toSelectStatement()
    {
        String statement = "SELECT animals.*, fig.name, fig.originId FROM " + TABLE + " animals";
        statement += " INNER JOIN " + Figure.TABLE + " fig";
        statement += " ON animals.figureId = fig.figureId";

        return statement;
    }

}
