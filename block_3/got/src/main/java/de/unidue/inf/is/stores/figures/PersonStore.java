package de.unidue.inf.is.stores.figures;

import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.domain.figures.Relationship;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.LocationStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Person Store
 */
public class PersonStore extends BaseStore
{

    public PersonStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Person find(int personId, boolean includePets)
    {
        return find(personId, includePets, false);
    }

    /**
     * Returns a person
     */
    public Person find(int personId, boolean includePets, boolean includeRelationships)
    {
        String statement = Person.toSelectStatement();
        try
        {
            statement += " WHERE per.personId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, personId);
            ResultSet resultSet = preparedStatement.executeQuery();

            Person person = null;

            if (resultSet.next())
            {
                person = new Person(resultSet);
                person = getObject(person, includePets, includeRelationships);
            }

            resultSet.close();
            preparedStatement.close();

            return person;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns the Person with the associated Figure Id
     */
    public Person findByFigureId(int figureId)
    {
        String statement = Person.toSelectStatement();
        try
        {
            statement += " WHERE per.figureId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, figureId);
            ResultSet resultSet = preparedStatement.executeQuery();

            Person person = null;

            if (resultSet.next())
            {
                person = new Person(resultSet);
                person = getObject(person, true, true);
            }

            resultSet.close();
            preparedStatement.close();

            return person;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Returns the full instantiated Person object
     */
    private Person getObject(Person person, boolean includePets, boolean includeRelationships)
    {
        LocationStore locationStore = new LocationStore();

        person.setOrigin(locationStore.find(person.getOriginId()));

        if (includePets)
        {
            AnimalStore animalStore = new AnimalStore();
            person.setPets(animalStore.findPetsFrom(person));

            animalStore.commit();
            animalStore.close();
        }

        locationStore.commit();
        locationStore.close();

        if (includeRelationships)
        {
            RelationshipStore relationshipStore = new RelationshipStore();
            List<Relationship> relationships = relationshipStore.find(person);
            person.setRelationships(relationships);

            relationshipStore.commit();
            relationshipStore.close();
        }

        return person;
    }

}
