package de.unidue.inf.is.domain.film;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Episode entity
 */
public class Episode
{
    public static final String TABLE = "EPISODES";
    public static final String LOCATION_RELATED_TABLE = "LOCATIONSINEPISODE";

    private int episodeId;
    private int seasonId;
    private int number;
    private Date date;
    private String title;
    private String summary;
    private Season season;

    public Episode() {}

    public Episode(ResultSet set)
            throws SQLException
    {
        this.episodeId = set.getInt("episodeId");
        this.seasonId = set.getInt("seasonId");
        this.number = set.getInt("number");
        this.date = set.getDate("date");
        this.title = set.getString("title");
        this.summary = set.getString("summary");
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getSummary() {

        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getSeasonId() {

        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumber() {

        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getEpisodeId() {

        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }
}
