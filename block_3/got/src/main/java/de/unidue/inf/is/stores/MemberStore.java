package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.HouseMember;
import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.stores.figures.PersonStore;
import de.unidue.inf.is.stores.film.EpisodeStore;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Members Store
 */
public class MemberStore extends BaseStore
{

    public MemberStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Finds the members of a given house
     */
    public List<HouseMember> findMembersFrom(int houseId)
    {
        try
        {
            String statement = HouseMember.toSelectStatement();
            statement += " WHERE members.houseId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, houseId);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<HouseMember> members = getObjects(resultSet);

            preparedStatement.close();
            return members;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * gets the memberships based on the personId
     */
    public List<HouseMember> findMembershipsFrom(int personId)
    {
        try
        {
            String statement = HouseMember.toSelectStatement();
            statement += " WHERE members.personId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, personId);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<HouseMember> memberships =  getObjects(resultSet);
            preparedStatement.close();

            return memberships;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Gets the memberships from the Person
     */
    public List<HouseMember> findMembershipsFrom(Person person)
    {
        try
        {
            String statement = HouseMember.toSelectStatement();
            statement += " WHERE members.personId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, person.getPersonId());
            ResultSet resultSet = preparedStatement.executeQuery();

            List<HouseMember> memberships =  getObjects(resultSet);
            preparedStatement.close();

            return memberships;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    private List<HouseMember> getObjects(ResultSet resultSet)
            throws SQLException
    {
        List<HouseMember> members = new ArrayList<>();

        while (resultSet.next())
        {
            HouseMember member = new HouseMember(resultSet);

            EpisodeStore episodeStore = new EpisodeStore();
            HouseStore houseStore = new HouseStore();
            PersonStore personStore = new PersonStore();

            // populate the member object -- REALLY EXPENSIVE CALL
            member.setFromEpisode(episodeStore.find(member.getFromEpisodeId()));
            member.setToEpisode(episodeStore.find(member.getToEpisodeId()));
            member.setHouse(houseStore.find(member.getHouseId()));
            member.setPerson(personStore.find(member.getPersonId(), false));

            episodeStore.close();
            houseStore.close();
            personStore.close();

            members.add(member);
        }

        return members;
    }

}
