package de.unidue.inf.is.servlets.api.playlists;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.domain.playlist.Playlist;
import de.unidue.inf.is.stores.UserStore;
import de.unidue.inf.is.stores.playlist.PlaylistStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Playlist Servlet
 */
public class PlaylistServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        User user = AuthenticatorAgent.getUser(request);
        // checks if there's a User attached to the current Session
        if(user == null)
        {
            response.sendRedirect("/login");  // required a user for the playlists
            return;
        }

        Gson gson = new Gson();

        String catalogString = request.getParameter("catalog");
        boolean catalog = (catalogString != null && (catalogString.equals("true") || catalogString.equals("1")));
        String playlistIdString = request.getParameter("playlistId");
        int playlistId = (playlistIdString != null && !playlistIdString.isEmpty()) ? Integer.parseInt(playlistIdString) : -1;
        String limitString = request.getParameter("limit");
        int limit = (limitString != null && !limitString.isEmpty()) ? Integer.parseInt(limitString) : -1;

        PlaylistStore playlistStore = new PlaylistStore();
        // if a playlistId is given then return only the playlist
        // if the catalog flag is set, then return full contents of the playlist

        String payload;

        if (playlistId >= 0)
        {
            // playlist id was given
            Playlist playlist = playlistStore.find(playlistId, catalog);
            payload = gson.toJson(playlist);
        } else {
            // return all playlist from the user
            List<Playlist> playlists = playlistStore.findAll(user.getId(), catalog, limit);
            payload = gson.toJson(playlists);
        }

        playlistStore.close();
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        response.getWriter().write(payload);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        User user = AuthenticatorAgent.getUser(request);
        if (user == null)
        {
            response.sendRedirect("/login");
            return;
        }

        String playlistName = request.getParameter("title");

        PlaylistStore playlistStore = new PlaylistStore();
        Playlist playlist = playlistStore.create(playlistName, user.getId());
        playlistStore.commit();
        playlistStore.close();
        Gson gson = new Gson();
        String payload = gson.toJson(playlist);

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }

}
