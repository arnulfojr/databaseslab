package de.unidue.inf.is.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Location Entity
 */
public class Location
{

    public static final String TABLE = "LOCATIONS";
    public static final String EPISODE_RELATED_TABLE = "LOCATIONSINEPISODE";

    private String name;
    private int locationId;

    public Location() {};

    public Location(ResultSet set)
            throws SQLException
    {
        this.name = set.getString("name");
        this.locationId = set.getInt("locationId");
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
