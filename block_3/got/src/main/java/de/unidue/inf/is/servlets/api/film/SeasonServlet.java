package de.unidue.inf.is.servlets.api.film;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.film.Season;
import de.unidue.inf.is.stores.film.SeasonStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Season servlet
 */
public class SeasonServlet extends HttpServlet
{

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String seasonIdString = request.getParameter("seasonId");
        Integer seasonId = (seasonIdString == null) ? null : Integer.parseInt(seasonIdString);

        SeasonStore store = new SeasonStore();

        String payload;
        Gson gson = new Gson();

        if (seasonId == null)
        {
            List<Season> seasons = store.findAll();
            payload = gson.toJson(seasons);
        } else {
            Season season = store.find(seasonId);
            payload = gson.toJson(season);
        }

        store.commit();
        store.close();

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }

}
