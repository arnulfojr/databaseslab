package de.unidue.inf.is.utils;

import de.unidue.inf.is.utils.connection.ConnectionWrapperInterface;
import de.unidue.inf.is.utils.connection.IbmDbConnection;
import de.unidue.inf.is.utils.connection.MySqlConnection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 */
public final class ConnectionFactory
{

    private ConnectionFactory() {}

    /**
     * Simple call to get a connection
     */
    public static Connection getConnection()
        throws SQLException
    {
        return ConnectionFactory.getConnection("database");
    }

    /**
     * @param database Name of the database to connect
     * @return Connection Instance
     * @throws SQLException
     */
    public static Connection getConnection(String database)
            throws SQLException
    {
        String databaseType = System.getenv("DB_TYPE");
        ConnectionWrapperInterface connectionWrapper;

        if (databaseType.equals("MYSQL"))
        {
            connectionWrapper = new MySqlConnection();
        } else {
            connectionWrapper = new IbmDbConnection();
        }

        connectionWrapper.registerDriver();
        return connectionWrapper.getConnection(database);
    }

    /**
     * Checks the database exists
     * @param database String representation of the database
     * @return
     */
    public static boolean checkDatabaseExists(String database)
    {
        try (Connection connection = getConnection(database)) {
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}
