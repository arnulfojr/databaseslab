package de.unidue.inf.is.stores.playlist;

import de.unidue.inf.is.domain.film.Episode;
import de.unidue.inf.is.domain.playlist.Item;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.stores.film.EpisodeStore;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Item Store
 */
public class ItemStore extends BaseStore
{
    public ItemStore()
    {
        try {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Item> findAll(int playlistId)
    {
        try
        {
            String stmt = "SELECT * FROM " + Item.TABLE + " WHERE playlistId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setInt(1, playlistId);
            List<Item> items = getList(preparedStatement);

            preparedStatement.close();

            return items;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Item find(int userId, int playlistId)
    {
        try
        {
            String stmt = "SELECT * FROM " + Item.TABLE + " WHERE playlistId = ? AND userId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setInt(1, playlistId);
            preparedStatement.setInt(2, userId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next())
            {

                Item item = new Item(resultSet);
                resultSet.close();
                preparedStatement.close();
                return item;
            }

            resultSet.close();
            preparedStatement.close();

            return null;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    private List<Item> getList(PreparedStatement statement)
            throws SQLException
    {
        ResultSet resultSet = statement.executeQuery();
        List<Item> items = new ArrayList<>();

        EpisodeStore episodeStore = new EpisodeStore();

        while (resultSet.next())
        {
            Item item = new Item(resultSet);
            Episode episode = episodeStore.find(item.getEpisodeId());
            item.setEpisode(episode);
            items.add(item);
        }

        episodeStore.commit();
        resultSet.close();

        return items;
    }

}
