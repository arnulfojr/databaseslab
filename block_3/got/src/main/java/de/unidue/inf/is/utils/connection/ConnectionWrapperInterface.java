package de.unidue.inf.is.utils.connection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface for connection wrappers
 */
public interface ConnectionWrapperInterface
{
    void registerDriver();

    Connection getConnection()
            throws SQLException;

    Connection getConnection(String database)
            throws SQLException;

}
