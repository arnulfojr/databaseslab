package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.Ruled;
import de.unidue.inf.is.stores.film.EpisodeStore;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Ruled locations store
 */
public class RuledStore extends BaseStore
{
    public RuledStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Finds the ruled locations by house Id
     */
    public List<Ruled> findByHouseId(int houseId)
    {
        try
        {
            String statement = "SELECT * FROM " + Ruled.TABLE;
            statement += " WHERE houseId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, houseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Ruled> ruledLocations = getObjects(resultSet);
            preparedStatement.close();
            return ruledLocations;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Loads the results as objects
     */
    private List<Ruled> getObjects(ResultSet resultSet)
            throws SQLException
    {
        List<Ruled> ruledList = new ArrayList<>();

        while (resultSet.next())
        {
            Ruled ruled = new Ruled(resultSet);
            LocationStore locationStore = new LocationStore();
            EpisodeStore episodeStore = new EpisodeStore();
            HouseStore houseStore = new HouseStore();

            ruled.setFromEpisode(episodeStore.find(ruled.getFromEpisodeId()));
            ruled.setToEpisode(episodeStore.find(ruled.getToEpisodeId()));
            ruled.setLocation(locationStore.find(ruled.getLocationId()));
            ruled.setHouse(houseStore.find(ruled.getHouseId(), true));

            ruledList.add(ruled);

            locationStore.commit();
            locationStore.close();

            episodeStore.commit();
            episodeStore.close();

            houseStore.commit();
            houseStore.close();
        }

        resultSet.close();

        return ruledList;
    }

}
