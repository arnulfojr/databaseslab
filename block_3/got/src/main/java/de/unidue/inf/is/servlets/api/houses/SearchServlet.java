package de.unidue.inf.is.servlets.api.houses;

import com.google.gson.Gson;
import de.unidue.inf.is.domain.House;
import de.unidue.inf.is.stores.HouseStore;
import de.unidue.inf.is.utils.AuthenticatorAgent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Search servlet for houses
 */
public class SearchServlet extends HttpServlet
{
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {

        if (!AuthenticatorAgent.isLogged(request))
        {
            response.sendRedirect("/login");
            return;
        }

        String keyword = request.getParameter("keyword");
        HouseStore houseStore = new HouseStore();
        List<House> houses = houseStore.search(keyword);

        houseStore.close();
        Gson gson = new Gson();
        String payload = gson.toJson(houses);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(payload);
    }
}
