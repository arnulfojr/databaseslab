package de.unidue.inf.is.domain.playlist;

import de.unidue.inf.is.domain.film.Episode;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Item Entity alias PlaylistContent
 */
public class Item
{
    public static final String TABLE = "PLAYLISTCONTENT";

    private int playlistId;
    private int userId;
    private int episodeId;
    private Episode episode;
    private int sequence;

    public Item(ResultSet resultSet)
            throws SQLException
    {
        this.playlistId = resultSet.getInt("playlistId");
        this.userId = resultSet.getInt("userId");
        this.episodeId = resultSet.getInt("episodeId");
        this.sequence = resultSet.getInt("sequence");
    }

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public int getUserId() {

        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSequence() {

        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getEpisodeId() {

        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }

    public int getPlaylistId() {

        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }
}
