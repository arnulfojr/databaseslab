package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.Figure;
import de.unidue.inf.is.domain.Location;
import de.unidue.inf.is.domain.figures.Animal;
import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.stores.figures.AnimalStore;
import de.unidue.inf.is.stores.figures.PersonStore;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Figure Store for querying the database
 */
public class FigureStore extends BaseStore
{

    /**
     * Constructor gets a connection
     */
    public FigureStore()
    {
        try
        {
            connection = ConnectionFactory.getConnection();
            connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Figure> search(String keyword)
    {
        try
        {
            if (keyword == null || keyword.isEmpty()) return findAll(1000);
            String stmt = "SELECT * FROM " + Figure.TABLE
                    + " WHERE NAME LIKE ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(stmt);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setMaxRows(1000);
            List<Figure> figures = getList(preparedStatement);

            preparedStatement.close();

            return figures;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public Figure guess(int specificId)
    {
        AnimalStore animalStore = new AnimalStore();
        Animal animal = animalStore.findByFigureId(specificId);
        animalStore.close();

        if (animal == null)
        {
            PersonStore personStore = new PersonStore();
            Person person = personStore.findByFigureId(specificId);
            personStore.close();
            return person;
        }

        return animal;
    }

    /**
     * Returns a list of Figures
     */
    public List<Figure> findAll(int limit)
    {
        limit = (limit < 1) ? 1000 : limit;  // set a default
        ResultSet set = null;
        try
        {
            String statement = "SELECT * FROM " + Figure.TABLE;

            PreparedStatement pStmt = this.connection.prepareStatement(statement);
            pStmt.setMaxRows(limit);
            List<Figure> figures = getList(pStmt);

            pStmt.close();

            return figures;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Figure> findByEpisodeId(int episodeId)
    {
        try
        {
            String selectStatement = Figure.selectStatement();
            selectStatement += " INNER JOIN " + Figure.IN_EPISODE_TABLE + " figep";
            selectStatement += " ON fig.figureId = figep.figureId";
            selectStatement += " WHERE figep.episodeId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(selectStatement);
            preparedStatement.setInt(1, episodeId);
            List<Figure> figures = getList(preparedStatement);

            preparedStatement.close();

            return figures;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    public List<Figure> findByOriginId(int locationId)
    {
        try
        {
            String statement = Figure.selectStatement();
            statement += " WHERE fig.originId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, locationId);
            List<Figure> figures = getList(preparedStatement);

            preparedStatement.close();

            return figures;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    private List<Figure> getList(PreparedStatement preparedStatement)
            throws SQLException
    {
        List<Figure> figures = new ArrayList<>();
        ResultSet set = preparedStatement.executeQuery();

        while (set.next())
        {
            LocationStore locationStore = new LocationStore(); // inefficient but ok
            Figure temp = new Figure(set);
            Location tempLocation = locationStore.find(temp.getOriginId());
            locationStore.commit(); // here's the commit it shouldn't be there
            temp.setOrigin(tempLocation);
            figures.add(temp);
        }
        set.close();

        return figures;
    }

}
