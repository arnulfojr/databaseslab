package de.unidue.inf.is.stores.figures;

import de.unidue.inf.is.domain.figures.Person;
import de.unidue.inf.is.domain.figures.Relationship;
import de.unidue.inf.is.stores.BaseStore;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.utils.ConnectionFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Relationship store
 */
public class RelationshipStore extends BaseStore
{
    public RelationshipStore()
    {
        try
        {
            this.connection = ConnectionFactory.getConnection();
            this.connection.setAutoCommit(false);
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Finds the relationships from the Person given
     */
    public List<Relationship> find(Person person)
    {
        return find(person.getPersonId());
    }

    /**
     * Finds the relationships from the personId given
     */
    public List<Relationship> find(int personId)
    {
        try
        {
            String statement = Relationship.toSelectStatement();
            statement += " WHERE rel.sourcePersonId = ?";
            PreparedStatement preparedStatement = this.connection.prepareStatement(statement);
            preparedStatement.setInt(1, personId);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Relationship> relationships = getObjects(resultSet);

            preparedStatement.close();

            return relationships;
        }
        catch (SQLException e)
        {
            throw new StoreException(e);
        }
    }

    /**
     * Get full instances of the Relationship entity
     */
    private List<Relationship> getObjects(ResultSet resultSet)
            throws SQLException
    {
        List<Relationship> relationships = new ArrayList<>();

        while (resultSet.next())
        {
            Relationship relationship = new Relationship(resultSet);
            PersonStore personStore = new PersonStore();

            relationship.setToWhom(personStore.find(relationship.getToWhomId(), false, false));

            personStore.commit();
            personStore.close();

            relationships.add(relationship);
        }

        resultSet.close();

        return relationships;
    }

}
