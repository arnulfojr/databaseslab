package de.unidue.inf.is.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Castle Entity
 */
public class Castle
{
    public static final String TABLE = "CASTLES";

    private int castleId;
    // points to a location
    private int locationId;
    private Location location;
    private String name;

    public Castle() {};

    /**
     * Create a new Castle based on a single row of a result set
     */
    public Castle(ResultSet set)
            throws SQLException
    {
        if (!set.next()) return;
        this.castleId = set.getInt("castleId");
        this.name = set.getString("name");
        this.locationId = set.getInt("locationId");
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocationId() {

        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getCastleId() {

        return castleId;
    }

    public void setCastleId(int castleId) {
        this.castleId = castleId;
    }
}
