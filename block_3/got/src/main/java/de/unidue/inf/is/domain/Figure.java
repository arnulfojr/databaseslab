package de.unidue.inf.is.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Figure entity
 */
public class Figure implements Storable
{

    public static final String TABLE = "FIGURES";
    public static final String IN_EPISODE_TABLE = "FIGURESINEPISODE";

    private int figureId;
    private String name;
    private int originId;
    private Location origin;

    public Figure() {}

    /**
     * Creates a Figure based on a single result in the ResultSet
     */
    public Figure(ResultSet set)
        throws SQLException
    {
        this.name = set.getString("name");
        this.figureId = set.getInt("figureId");
        this.originId = set.getInt("originId");
    }

    /**
     * Creates a new Figure with the given name
     */
    public Figure(String name)
    {
        this.name = name;
    };

    public int getFigureId() {
        return figureId;
    }

    public void setFigureId(int figureId) {
        this.figureId = figureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOriginId() {
        return originId;
    }

    public void setOriginId(int originId) {
        this.originId = originId;
    }

    public Location getOrigin() {
        return origin;
    }

    public void setOrigin(Location origin) {
        this.origin = origin;
    }

    @Override
    public String toInsertStatement() {
        return "INSERT INTO "
                + TABLE
                + " (name, figureId, originId)"
                + " VALUES (?, DEFAULT, ?)";
    }

    public static String selectStatement() {
        return "SELECT fig.figureId, fig.originId, fig.name FROM " + TABLE + " fig";
    }
}
