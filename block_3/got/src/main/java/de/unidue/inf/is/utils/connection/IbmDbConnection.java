package de.unidue.inf.is.utils.connection;

import com.ibm.db2.jcc.DB2BaseDataSource;
import com.ibm.db2.jcc.DB2Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * IBM DB2 connection wrapper
 */
public class IbmDbConnection implements ConnectionWrapperInterface
{

    @Override
    public void registerDriver()
    {
        try
        {
            DB2Driver driver = new DB2Driver();
            DriverManager.registerDriver(driver);
        }
        catch (SQLException e)
        {
            throw new Error("DB2 driver couldn't be loaded");
        }
    }

    @Override
    public Connection getConnection()
            throws SQLException
    {
        return this.getConnection("gameofthrones");
    }

    @Override
    public Connection getConnection(String database)
            throws SQLException
    {
        Properties properties = new Properties();
        properties.setProperty("user", "db2inst1");
        properties.setProperty("password", "mydbpassword");
        properties.setProperty(
                "retryWithAlternativeSecurityMechanism",
                Integer.toString(DB2BaseDataSource.YES)
        );
        properties.setProperty("securityMechanism",
                Integer.toString(DB2BaseDataSource.CLEAR_TEXT_PASSWORD_SECURITY)
        );
        String url = "jdbc:db2://0.0.0.0:50000/" + database;
        url += ":currentSchema=GAMEOFTHRONES;";
        return DriverManager.getConnection(url, properties);
    }
}
