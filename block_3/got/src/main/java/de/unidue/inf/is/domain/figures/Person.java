package de.unidue.inf.is.domain.figures;

import de.unidue.inf.is.domain.Figure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Person Entity
 */
public class Person extends Figure
{

    public final static String TABLE = "PERSON";

    private int personId;
    private String title;
    private String bibliography;
    private List<Animal> pets;
    private List<Relationship> relationships;

    public Person()
    {
        super();
    }

    public Person(ResultSet resultSet)
        throws SQLException
    {
        super(resultSet);
        this.personId = resultSet.getInt("personId");
        this.title = resultSet.getString("title");
        this.bibliography = resultSet.getString("bibliography");
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBibliography() {
        return bibliography;
    }

    public void setBibliography(String bibliography) {
        this.bibliography = bibliography;
    }

    public List<Animal> getPets() {
        return pets;
    }

    public void setPets(List<Animal> pets) {
        this.pets = pets;
    }

    public List<Relationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(List<Relationship> relationships) {
        this.relationships = relationships;
    }

    public static String toSelectStatement()
    {
        String statement = "SELECT per.*, fig.originId, fig.name FROM " + TABLE + " per";
        statement += " INNER JOIN " + Figure.TABLE + " fig ON ";
        statement += "fig.FIGUREID = per.FIGUREID";

        return statement;
    }

}
