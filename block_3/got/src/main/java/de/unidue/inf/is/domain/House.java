package de.unidue.inf.is.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * House Entity
 */
public class House
{
    public static final String TABLE = "HOUSE";
    public static final String RULED_TABLED = "RULEDLOCATIONS";

    private int houseId;
    private String name;
    private String motto;
    private String wappen;
    // points to the castle the sit in
    private int seatsInId;
    private Castle castle;

    public House() {}

    public House(ResultSet set)
        throws SQLException
    {
        this.houseId = set.getInt("houseId");
        this.name = set.getString("name");
        this.motto = set.getString("motto");
        this.wappen = set.getString("wappen");
        this.seatsInId = set.getInt("seatsInId");
    }

    public Castle getCastle() {
        return castle;
    }

    public void setCastle(Castle castle) {
        this.castle = castle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeatsInId() {

        return seatsInId;
    }

    public void setSeatsInId(int seatsInId) {
        this.seatsInId = seatsInId;
    }

    public String getWappen() {

        return wappen;
    }

    public void setWappen(String wappen) {
        this.wappen = wappen;
    }

    public String getMotto() {

        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public int getHouseId() {

        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }
}
